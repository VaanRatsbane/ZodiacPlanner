﻿namespace ZodiacPlanner
{
    partial class CustomLic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomLic));
            this.loadVBF = new System.Windows.Forms.Button();
            this.loadBP = new System.Windows.Forms.Button();
            this.loadBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tradchinese = new System.Windows.Forms.RadioButton();
            this.simplechinese = new System.Windows.Forms.RadioButton();
            this.korean = new System.Windows.Forms.RadioButton();
            this.japanese = new System.Windows.Forms.RadioButton();
            this.spanish = new System.Windows.Forms.RadioButton();
            this.german = new System.Windows.Forms.RadioButton();
            this.italian = new System.Windows.Forms.RadioButton();
            this.french = new System.Windows.Forms.RadioButton();
            this.english = new System.Windows.Forms.RadioButton();
            this.openVbfDialog = new System.Windows.Forms.OpenFileDialog();
            this.openBinDialog = new System.Windows.Forms.OpenFileDialog();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // loadVBF
            // 
            this.loadVBF.Location = new System.Drawing.Point(12, 27);
            this.loadVBF.Name = "loadVBF";
            this.loadVBF.Size = new System.Drawing.Size(115, 23);
            this.loadVBF.TabIndex = 0;
            this.loadVBF.Text = "From VBF";
            this.loadVBF.UseVisualStyleBackColor = true;
            this.loadVBF.Click += new System.EventHandler(this.loadVBF_Click);
            // 
            // loadBP
            // 
            this.loadBP.Location = new System.Drawing.Point(147, 27);
            this.loadBP.Name = "loadBP";
            this.loadBP.Size = new System.Drawing.Size(115, 23);
            this.loadBP.TabIndex = 1;
            this.loadBP.Text = "From battle_pack.bin";
            this.loadBP.UseVisualStyleBackColor = true;
            this.loadBP.Click += new System.EventHandler(this.loadBP_Click);
            // 
            // loadBtn
            // 
            this.loadBtn.Enabled = false;
            this.loadBtn.Location = new System.Drawing.Point(52, 228);
            this.loadBtn.Name = "loadBtn";
            this.loadBtn.Size = new System.Drawing.Size(75, 23);
            this.loadBtn.TabIndex = 2;
            this.loadBtn.Text = "Load";
            this.loadBtn.UseVisualStyleBackColor = true;
            this.loadBtn.Click += new System.EventHandler(this.loadBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(147, 228);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 3;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Language (only supporting english for now)";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tradchinese, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.simplechinese, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.korean, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.japanese, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.spanish, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.german, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.italian, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.french, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.english, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(15, 78);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(247, 144);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // tradchinese
            // 
            this.tradchinese.AutoSize = true;
            this.tradchinese.Enabled = false;
            this.tradchinese.Location = new System.Drawing.Point(3, 115);
            this.tradchinese.Name = "tradchinese";
            this.tradchinese.Size = new System.Drawing.Size(94, 17);
            this.tradchinese.TabIndex = 8;
            this.tradchinese.Text = "(ch) 繁體中文";
            this.tradchinese.UseVisualStyleBackColor = true;
            // 
            // simplechinese
            // 
            this.simplechinese.AutoSize = true;
            this.simplechinese.Enabled = false;
            this.simplechinese.Location = new System.Drawing.Point(126, 87);
            this.simplechinese.Name = "simplechinese";
            this.simplechinese.Size = new System.Drawing.Size(94, 17);
            this.simplechinese.TabIndex = 7;
            this.simplechinese.Text = "(cn) 简体中文";
            this.simplechinese.UseVisualStyleBackColor = true;
            // 
            // korean
            // 
            this.korean.AutoSize = true;
            this.korean.Enabled = false;
            this.korean.Location = new System.Drawing.Point(3, 87);
            this.korean.Name = "korean";
            this.korean.Size = new System.Drawing.Size(76, 17);
            this.korean.TabIndex = 6;
            this.korean.Text = "(kr) 한국어";
            this.korean.UseVisualStyleBackColor = true;
            // 
            // japanese
            // 
            this.japanese.AutoSize = true;
            this.japanese.Enabled = false;
            this.japanese.Location = new System.Drawing.Point(126, 59);
            this.japanese.Name = "japanese";
            this.japanese.Size = new System.Drawing.Size(78, 17);
            this.japanese.TabIndex = 5;
            this.japanese.Text = "(jp) 日本人";
            this.japanese.UseVisualStyleBackColor = true;
            // 
            // spanish
            // 
            this.spanish.AutoSize = true;
            this.spanish.Enabled = false;
            this.spanish.Location = new System.Drawing.Point(3, 59);
            this.spanish.Name = "spanish";
            this.spanish.Size = new System.Drawing.Size(83, 17);
            this.spanish.TabIndex = 4;
            this.spanish.Text = "(es) Español";
            this.spanish.UseVisualStyleBackColor = true;
            // 
            // german
            // 
            this.german.AutoSize = true;
            this.german.Enabled = false;
            this.german.Location = new System.Drawing.Point(126, 31);
            this.german.Name = "german";
            this.german.Size = new System.Drawing.Size(86, 17);
            this.german.TabIndex = 3;
            this.german.Text = "(de) Deutsch";
            this.german.UseVisualStyleBackColor = true;
            // 
            // italian
            // 
            this.italian.AutoSize = true;
            this.italian.Enabled = false;
            this.italian.Location = new System.Drawing.Point(3, 31);
            this.italian.Name = "italian";
            this.italian.Size = new System.Drawing.Size(73, 17);
            this.italian.TabIndex = 2;
            this.italian.Text = "(it) Italiano";
            this.italian.UseVisualStyleBackColor = true;
            // 
            // french
            // 
            this.french.AutoSize = true;
            this.french.Enabled = false;
            this.french.Location = new System.Drawing.Point(126, 3);
            this.french.Name = "french";
            this.french.Size = new System.Drawing.Size(86, 17);
            this.french.TabIndex = 1;
            this.french.Text = "(fr) Française";
            this.french.UseVisualStyleBackColor = true;
            // 
            // english
            // 
            this.english.AutoSize = true;
            this.english.Checked = true;
            this.english.Enabled = false;
            this.english.Location = new System.Drawing.Point(3, 3);
            this.english.Name = "english";
            this.english.Size = new System.Drawing.Size(79, 17);
            this.english.TabIndex = 0;
            this.english.TabStop = true;
            this.english.Text = "(us) English";
            this.english.UseVisualStyleBackColor = true;
            // 
            // CustomLic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(273, 263);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.loadBtn);
            this.Controls.Add(this.loadBP);
            this.Controls.Add(this.loadVBF);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CustomLic";
            this.Text = "Loading Custom Licences";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button loadVBF;
        private System.Windows.Forms.Button loadBP;
        private System.Windows.Forms.Button loadBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.RadioButton tradchinese;
        private System.Windows.Forms.RadioButton simplechinese;
        private System.Windows.Forms.RadioButton korean;
        private System.Windows.Forms.RadioButton japanese;
        private System.Windows.Forms.RadioButton spanish;
        private System.Windows.Forms.RadioButton german;
        private System.Windows.Forms.RadioButton italian;
        private System.Windows.Forms.RadioButton french;
        private System.Windows.Forms.RadioButton english;
        private System.Windows.Forms.OpenFileDialog openVbfDialog;
        private System.Windows.Forms.OpenFileDialog openBinDialog;
    }
}