﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZodiacPlanner
{
    public class BattlePack
    {
        byte[] originalBytes;

        List<byte[]> SubFiles;

        public int SubFileCount { get { return SubFiles.Count; } }
        public byte[] GetSubFile(int index) => SubFiles[index];
        public void UpdateSubFile(int index, byte[] NewBytes) => SubFiles[index] = NewBytes;

        public BattlePack(byte[] FileBytes)
        {
            originalBytes = FileBytes;

            int posStart = 0x004;
            int posEnd = 0x124;

            var positions = new List<int>();
            for (int i = posStart; i < posEnd; i += 4)
            {
                var position = BitConverter.ToInt32(FileBytes, i);
                if (!positions.Contains(position))
                    positions.Add(position);
            }

            SubFiles = new List<byte[]>();

            for (var i = 0; i < positions.Count() - 1; i++)
            {
                var position = positions[i];
                int end = positions[i + 1];
                end = i + 1 == positions.Count() ? FileBytes.Length : positions[i + 1];
                var fileArray = new byte[end - position - 4];
                Array.Copy(FileBytes, position, fileArray, 0, end - position - 4); //always -4 because endings of battlepack are 4 0x00
                SubFiles.Add(fileArray);
            }
        }
    }

    public class WordPack
    {
        byte[] originalBytes;

        List<byte[]> SubFiles;

        public int SubFileCount { get { return SubFiles.Count; } }
        public byte[] GetSubFile(int index) => SubFiles[index];
        public void UpdateSubFile(int index, byte[] NewBytes) => SubFiles[index] = NewBytes;

        public WordPack(byte[] FileBytes)
        {
            originalBytes = FileBytes;

            var sectionAmount = BitConverter.ToInt32(originalBytes, 0);
            var sectionStarts = new List<int>();
            int pos = 0x04;
            while (pos < 0x04 + sectionAmount * 4)
            {
                sectionStarts.Add(BitConverter.ToInt32(originalBytes, pos));
                pos += 0x04;
            }
            int EOF = BitConverter.ToInt32(originalBytes, pos); //after the offsets, there's a final position

            SubFiles = new List<byte[]>();

            for (int processed = 0; processed < sectionAmount; processed++)
            {
                var newBytes = new List<byte>();
                var start = sectionStarts[processed];
                var end = processed + 1 < sectionAmount ? sectionStarts[processed + 1] : EOF;

                for (int i = start; i < end; i++)
                    newBytes.Add(originalBytes[i]);

                SubFiles.Add(newBytes.ToArray());
            }
        }
    }

    public class WordFile
    {

        byte[] originalBytes;
        int blockStart;
        int stringCount;
        int stringsStart;

        public WordFile(byte[] FileBytes)
        {
            originalBytes = FileBytes;
            blockStart = 0x04;
            stringCount = BitConverter.ToInt32(originalBytes, 0x00);
            stringsStart = blockStart + (stringCount + 1) * 4;
        }

        public string[] Decode(Dictionary<byte, char> Decoder)
        {
            var positions = new List<int>();
            for (int i = blockStart; i < stringsStart; i += 4)
            {
                var position = BitConverter.ToInt32(originalBytes, i);

                if (!positions.Contains(position))
                    positions.Add(position);
            }

            var output = String.Empty;
            for (int i = 0; i < positions.Count - 1; i++)
            {
                var start = positions[i];
                var end = positions[i + 1];
                var line = String.Empty;
                for (int p = start; p < end; p++)
                {
                    var bt = originalBytes[p];
                    if (Decoder.ContainsKey(bt))
                        line += Decoder[bt];
                    else
                        line += "{" + bt.ToString("X2") + "}";
                }
                output += line + '\n';
            }

            return output.Substring(0, output.Length - 1).Split('\n');
        }
    }
}