﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZodiacPlanner
{
    public class LoadedStrings
    {
        Dictionary<ushort, string> gear, abilities, augs, summons;
        List<string> names;
        string essentials;

        public LoadedStrings(IEnumerable<string> names, IEnumerable<string> gear, IEnumerable<string> abilities, IEnumerable<string> augments)
        {
            SetupLicenceNames(names);
            SetupGear(gear);
            SetupAbilities(abilities);
            SetupAugs(augments);
        }


        void SetupGear (IEnumerable<string> gear)
        {
            this.gear = new Dictionary<ushort, string>();
            this.gear.Add(0x1001, gear.ElementAt(1)); //Broadsword
            this.gear.Add(0x1002, gear.ElementAt(2)); //Longsword
            this.gear.Add(0x1003, gear.ElementAt(3)); //Iron Sword
            this.gear.Add(0x1004, gear.ElementAt(4)); //Zwill Blade
            this.gear.Add(0x1005, gear.ElementAt(5)); //Ancient Sword
            this.gear.Add(0x1006, gear.ElementAt(6)); //Blood Sword
            this.gear.Add(0x1007, gear.ElementAt(7)); //Lohengrin
            this.gear.Add(0x1008, gear.ElementAt(8)); //Flametongue
            this.gear.Add(0x1009, gear.ElementAt(9)); //Demonsbane
            this.gear.Add(0x100A, gear.ElementAt(10)); //Icebrand
            this.gear.Add(0x100B, gear.ElementAt(11)); //Platinum Sword
            this.gear.Add(0x100C, gear.ElementAt(12)); //Bastard Sword
            this.gear.Add(0x100D, gear.ElementAt(13)); //Diamond Sword
            this.gear.Add(0x100E, gear.ElementAt(14)); //Runeblade
            this.gear.Add(0x1012, gear.ElementAt(18)); //Claymore
            this.gear.Add(0x1013, gear.ElementAt(19)); //Defender
            this.gear.Add(0x1014, gear.ElementAt(20)); //Save the Queen
            this.gear.Add(0x1016, gear.ElementAt(22)); //Ultima Blade
            this.gear.Add(0x1017, gear.ElementAt(23)); //Excalibur
            this.gear.Add(0x1018, gear.ElementAt(24)); //Tournesol
            this.gear.Add(0x1019, gear.ElementAt(25)); //Kotetsu
            this.gear.Add(0x101A, gear.ElementAt(26)); //Osafune
            this.gear.Add(0x101B, gear.ElementAt(27)); //Kogarasumaru
            this.gear.Add(0x101C, gear.ElementAt(28)); //Magoroku
            this.gear.Add(0x101D, gear.ElementAt(29)); //Murasame
            this.gear.Add(0x101E, gear.ElementAt(30)); //Kiku-ichimonji
            this.gear.Add(0x101F, gear.ElementAt(31)); //Yakei
            this.gear.Add(0x1020, gear.ElementAt(32)); //Ame-no-Murakumo
            this.gear.Add(0x1022, gear.ElementAt(34)); //Masamune
            this.gear.Add(0x1023, gear.ElementAt(35)); //Ashura
            this.gear.Add(0x1024, gear.ElementAt(36)); //Sakura-saezuri
            this.gear.Add(0x1025, gear.ElementAt(37)); //Kagenui
            this.gear.Add(0x1028, gear.ElementAt(40)); //Orochi
            this.gear.Add(0x1027, gear.ElementAt(39)); //Iga Blade
            this.gear.Add(0x1029, gear.ElementAt(41)); //Yagyu Darkblade
            this.gear.Add(0x10AB, gear.ElementAt(171)); //Mesa
            this.gear.Add(0x102A, gear.ElementAt(42)); //Javelin
            this.gear.Add(0x102B, gear.ElementAt(43)); //Spear
            this.gear.Add(0x102C, gear.ElementAt(44)); //Partisan
            this.gear.Add(0x102D, gear.ElementAt(45)); //Heavy Lance
            this.gear.Add(0x102E, gear.ElementAt(46)); //Storm Spear
            this.gear.Add(0x102F, gear.ElementAt(47)); //Obelisk
            this.gear.Add(0x1030, gear.ElementAt(48)); //Halberd
            this.gear.Add(0x1031, gear.ElementAt(49)); //Trident
            this.gear.Add(0x1032, gear.ElementAt(50)); //Holy Lance
            this.gear.Add(0x1033, gear.ElementAt(51)); //Gungnir
            this.gear.Add(0x1034, gear.ElementAt(52)); //Dragon Whisker
            this.gear.Add(0x1035, gear.ElementAt(53)); //Zodiac Spear
            this.gear.Add(0x1036, gear.ElementAt(54)); //Oaken Pole
            this.gear.Add(0x1037, gear.ElementAt(56)); //Battle Bamboo
            this.gear.Add(0x1038, gear.ElementAt(55)); //Cypress Pole
            this.gear.Add(0x1039, gear.ElementAt(57)); //Musk Stick
            this.gear.Add(0x103A, gear.ElementAt(58)); //Iron Pole
            this.gear.Add(0x103B, gear.ElementAt(59)); //Six-fluted Pole
            this.gear.Add(0x103C, gear.ElementAt(60)); //Gokuu Pole
            this.gear.Add(0x103D, gear.ElementAt(61)); //Zephyr Pole
            this.gear.Add(0x103E, gear.ElementAt(62)); //Ivory Pole
            this.gear.Add(0x1041, gear.ElementAt(65)); //Whale Whisker
            this.gear.Add(0x1042, gear.ElementAt(66)); //Shortbow
            this.gear.Add(0x1043, gear.ElementAt(67)); //Silver Bow
            this.gear.Add(0x1044, gear.ElementAt(68)); //Aevis Killer
            this.gear.Add(0x1045, gear.ElementAt(70)); //Longbow
            this.gear.Add(0x1046, gear.ElementAt(69)); //Killer Bow
            this.gear.Add(0x1047, gear.ElementAt(71)); //Elfin Bow
            this.gear.Add(0x1048, gear.ElementAt(72)); //Loxley Bow
            this.gear.Add(0x1049, gear.ElementAt(73)); //Giant Stonebow
            this.gear.Add(0x104A, gear.ElementAt(74)); //Burning Bow
            this.gear.Add(0x104B, gear.ElementAt(75)); //Traitor's Bow
            this.gear.Add(0x104C, gear.ElementAt(76)); //Yoichi Bow
            this.gear.Add(0x104F, gear.ElementAt(79)); //Sagittarius
            this.gear.Add(0x1050, gear.ElementAt(80)); //Bowgun
            this.gear.Add(0x1051, gear.ElementAt(81)); //Crossbow
            this.gear.Add(0x1052, gear.ElementAt(82)); //Paramina Crossbow
            this.gear.Add(0x1053, gear.ElementAt(83)); //Recurve Crossbow
            this.gear.Add(0x1054, gear.ElementAt(84)); //Hunting Crossbow
            this.gear.Add(0x1055, gear.ElementAt(85)); //Penetrator Crossbow
            this.gear.Add(0x1056, gear.ElementAt(86)); //Gastrophetes
            this.gear.Add(0x10B0, gear.ElementAt(176)); //Tula
            this.gear.Add(0x1057, gear.ElementAt(87)); //Altair
            this.gear.Add(0x1058, gear.ElementAt(88)); //Capella
            this.gear.Add(0x1059, gear.ElementAt(89)); //Vega
            this.gear.Add(0x105A, gear.ElementAt(90)); //Sirius
            this.gear.Add(0x105B, gear.ElementAt(91)); //Betelgeuse
            this.gear.Add(0x105C, gear.ElementAt(92)); //Ras Algethi
            this.gear.Add(0x105D, gear.ElementAt(93)); //Aldebaran
            this.gear.Add(0x105E, gear.ElementAt(94)); //Spica
            this.gear.Add(0x105F, gear.ElementAt(95)); //Antares
            this.gear.Add(0x1060, gear.ElementAt(96)); //Arcturus
            this.gear.Add(0x1061, gear.ElementAt(97)); //Fomalhaut
            this.gear.Add(0x1062, gear.ElementAt(98)); //Handaxe
            this.gear.Add(0x1069, gear.ElementAt(105)); //Iron Hammer
            this.gear.Add(0x1063, gear.ElementAt(99)); //Broadaxe
            this.gear.Add(0x106A, gear.ElementAt(106)); //War Hammer
            this.gear.Add(0x1064, gear.ElementAt(100)); //Slasher
            this.gear.Add(0x106B, gear.ElementAt(107)); //Sledgehammer
            this.gear.Add(0x1065, gear.ElementAt(101)); //Hammerhead
            this.gear.Add(0x1066, gear.ElementAt(102)); //Francisca
            this.gear.Add(0x106C, gear.ElementAt(108)); //Morning Star
            this.gear.Add(0x1067, gear.ElementAt(103)); //Greataxe
            this.gear.Add(0x1068, gear.ElementAt(104)); //Golden Axe
            this.gear.Add(0x106D, gear.ElementAt(109)); //Scorpion Tail
            this.gear.Add(0x106E, gear.ElementAt(110)); //Dagger
            this.gear.Add(0x106F, gear.ElementAt(111)); //Mage Masher
            this.gear.Add(0x1070, gear.ElementAt(112)); //Assassin's Dagger
            this.gear.Add(0x1071, gear.ElementAt(113)); //Chopper
            this.gear.Add(0x1072, gear.ElementAt(114)); //Main Gauche
            this.gear.Add(0x1073, gear.ElementAt(115)); //Gladius
            this.gear.Add(0x1074, gear.ElementAt(116)); //Avenger
            this.gear.Add(0x1075, gear.ElementAt(117)); //Orichalcum Dirk
            this.gear.Add(0x1076, gear.ElementAt(118)); //Platinum Dagger
            this.gear.Add(0x1078, gear.ElementAt(120)); //Shikari Nagasa
            this.gear.Add(0x10AC, gear.ElementAt(172)); //Mina
            this.gear.Add(0x1079, gear.ElementAt(121)); //Rod
            this.gear.Add(0x107A, gear.ElementAt(122)); //Serpent Rod
            this.gear.Add(0x107B, gear.ElementAt(123)); //Healing Rod
            this.gear.Add(0x107C, gear.ElementAt(124)); //Gaia Rod
            this.gear.Add(0x107D, gear.ElementAt(125)); //Power Rod
            this.gear.Add(0x107E, gear.ElementAt(126)); //Empyrean Rod
            this.gear.Add(0x107F, gear.ElementAt(127)); //Holy Rod
            this.gear.Add(0x1080, gear.ElementAt(128)); //Rod of Faith
            this.gear.Add(0x1081, gear.ElementAt(129)); //Oak Staff
            this.gear.Add(0x1082, gear.ElementAt(130)); //Cherry Staff
            this.gear.Add(0x1083, gear.ElementAt(131)); //Wizard's Staff
            this.gear.Add(0x1084, gear.ElementAt(132)); //Flame Staff
            this.gear.Add(0x1085, gear.ElementAt(133)); //Storm Staff
            this.gear.Add(0x1086, gear.ElementAt(134)); //Glacial Staff
            this.gear.Add(0x1087, gear.ElementAt(135)); //Golden Staff
            this.gear.Add(0x108A, gear.ElementAt(138)); //Staff of the Magi
            this.gear.Add(0x108B, gear.ElementAt(139)); //Mace
            this.gear.Add(0x108C, gear.ElementAt(140)); //Bronze Mace
            this.gear.Add(0x108D, gear.ElementAt(141)); //Bhuj
            this.gear.Add(0x108E, gear.ElementAt(142)); //Miter
            this.gear.Add(0x108F, gear.ElementAt(143)); //Thorned Mace
            this.gear.Add(0x1090, gear.ElementAt(144)); //Chaos Mace
            this.gear.Add(0x1091, gear.ElementAt(145)); //Doom Mace
            this.gear.Add(0x1092, gear.ElementAt(146)); //Zeus Mace
            this.gear.Add(0x1093, gear.ElementAt(147)); //Grand Mace
            this.gear.Add(0x10AF, gear.ElementAt(175)); //Bone of Byblos
            this.gear.Add(0x1094, gear.ElementAt(148)); //Gilt Measure
            this.gear.Add(0x1095, gear.ElementAt(149)); //Arc Scale
            this.gear.Add(0x1096, gear.ElementAt(150)); //Multiscale
            this.gear.Add(0x1097, gear.ElementAt(151)); //Cross Scale
            this.gear.Add(0x1098, gear.ElementAt(152)); //Caliper
            this.gear.Add(0x109A, gear.ElementAt(154)); //Hornito
            this.gear.Add(0x109B, gear.ElementAt(155)); //Fumarole
            this.gear.Add(0x109C, gear.ElementAt(156)); //Tumulus
            this.gear.Add(0x109D, gear.ElementAt(157)); //Caldera
            this.gear.Add(0x109E, gear.ElementAt(158)); //Volcano
            this.gear.Add(0x10C9, gear.ElementAt(201)); //Leather Shield
            this.gear.Add(0x10CA, gear.ElementAt(202)); //Buckler
            this.gear.Add(0x10CB, gear.ElementAt(203)); //Bronze Shield
            this.gear.Add(0x10CC, gear.ElementAt(204)); //Round Shield
            this.gear.Add(0x10CE, gear.ElementAt(206)); //Golden Shield
            this.gear.Add(0x10CF, gear.ElementAt(207)); //Ice Shield
            this.gear.Add(0x10D0, gear.ElementAt(208)); //Flame Shield
            this.gear.Add(0x10D1, gear.ElementAt(209)); //Diamond Shield
            this.gear.Add(0x10D2, gear.ElementAt(210)); //Platinum Shield
            this.gear.Add(0x10D3, gear.ElementAt(211)); //Dragon Shield
            this.gear.Add(0x10D4, gear.ElementAt(212)); //Crystal Shield
            this.gear.Add(0x10D6, gear.ElementAt(214)); //Kaiser Shield
            this.gear.Add(0x10D7, gear.ElementAt(215)); //Aegis Shield
            this.gear.Add(0x10D8, gear.ElementAt(216)); //Demon Shield
            this.gear.Add(0x10DB, gear.ElementAt(219)); //Ensanguined Shield
            this.gear.Add(0x10CD, gear.ElementAt(205)); //Shell Shield
            this.gear.Add(0x10DA, gear.ElementAt(218)); //Zodiac Escutcheon
            this.gear.Add(0x1106, gear.ElementAt(262)); //Leather Helm
            this.gear.Add(0x1107, gear.ElementAt(263)); //Bronze Helm
            this.gear.Add(0x1142, gear.ElementAt(322)); //Leather Armor
            this.gear.Add(0x1143, gear.ElementAt(324)); //Bronze Armor
            this.gear.Add(0x1108, gear.ElementAt(264)); //Sallet
            this.gear.Add(0x1109, gear.ElementAt(265)); //Iron Helm
            this.gear.Add(0x1144, gear.ElementAt(323)); //Scale Armor
            this.gear.Add(0x1145, gear.ElementAt(325)); //Iron Armor
            this.gear.Add(0x110A, gear.ElementAt(266)); //Barbut
            this.gear.Add(0x110B, gear.ElementAt(267)); //Winged Helm
            this.gear.Add(0x1146, gear.ElementAt(326)); //Linen Cuirass
            this.gear.Add(0x1147, gear.ElementAt(327)); //Chainmail
            this.gear.Add(0x110C, gear.ElementAt(268)); //Golden Helm
            this.gear.Add(0x110D, gear.ElementAt(269)); //Burgonet
            this.gear.Add(0x1148, gear.ElementAt(328)); //Golden Armor
            this.gear.Add(0x1149, gear.ElementAt(329)); //Shielded Armor
            this.gear.Add(0x110E, gear.ElementAt(270)); //Close Helmet
            this.gear.Add(0x110F, gear.ElementAt(272)); //Bone Helm
            this.gear.Add(0x114A, gear.ElementAt(330)); //Demon Mail
            this.gear.Add(0x114B, gear.ElementAt(331)); //Bone Mail
            this.gear.Add(0x1110, gear.ElementAt(273)); //Diamond Helm
            this.gear.Add(0x114C, gear.ElementAt(332)); //Diamond Armor
            this.gear.Add(0x1111, gear.ElementAt(271)); //Steel Mask
            this.gear.Add(0x114D, gear.ElementAt(333)); //Mirror Mail
            this.gear.Add(0x1112, gear.ElementAt(274)); //Platinum Helm
            this.gear.Add(0x114E, gear.ElementAt(334)); //Platinum Armor
            this.gear.Add(0x1113, gear.ElementAt(275)); //Giant's Helmet
            this.gear.Add(0x114F, gear.ElementAt(335)); //Carabineer Mail
            this.gear.Add(0x1114, gear.ElementAt(276)); //Dragon Helm
            this.gear.Add(0x1150, gear.ElementAt(336)); //Dragon Mail
            this.gear.Add(0x10D5, gear.ElementAt(213)); //Genji Shield
            this.gear.Add(0x1115, gear.ElementAt(278)); //Genji Helm
            this.gear.Add(0x1151, gear.ElementAt(337)); //Genji Armor
            this.gear.Add(0x1164, gear.ElementAt(356)); //Genji Gloves
            this.gear.Add(0x10DC, gear.ElementAt(220)); //Leather Cap
            this.gear.Add(0x1118, gear.ElementAt(280)); //Leather Clothing
            this.gear.Add(0x10DD, gear.ElementAt(221)); //Headgear
            this.gear.Add(0x10DE, gear.ElementAt(222)); //Headguard
            this.gear.Add(0x1119, gear.ElementAt(281)); //Chromed Leathers
            this.gear.Add(0x111A, gear.ElementAt(282)); //Leather Breastplate
            this.gear.Add(0x10DF, gear.ElementAt(223)); //Leather Headgear
            this.gear.Add(0x10E0, gear.ElementAt(224)); //Horned Hat
            this.gear.Add(0x111B, gear.ElementAt(283)); //Bronze Chestplate
            this.gear.Add(0x111C, gear.ElementAt(284)); //Ringmail
            this.gear.Add(0x10E1, gear.ElementAt(225)); //Balaclava
            this.gear.Add(0x10E2, gear.ElementAt(226)); //Soldier's Cap
            this.gear.Add(0x111D, gear.ElementAt(285)); //Windbreaker
            this.gear.Add(0x111E, gear.ElementAt(286)); //Heavy Coat
            this.gear.Add(0x10E3, gear.ElementAt(227)); //Green Beret
            this.gear.Add(0x10E4, gear.ElementAt(228)); //Red Cap
            this.gear.Add(0x111F, gear.ElementAt(287)); //Survival Vest
            this.gear.Add(0x1120, gear.ElementAt(288)); //Brigandine
            this.gear.Add(0x10E5, gear.ElementAt(229)); //Headband
            this.gear.Add(0x10E6, gear.ElementAt(230)); //Pirate Hat
            this.gear.Add(0x1121, gear.ElementAt(289)); //Jujitsu Gi
            this.gear.Add(0x1122, gear.ElementAt(290)); //Viking Coat
            this.gear.Add(0x10E7, gear.ElementAt(231)); //Goggle Mask
            this.gear.Add(0x10E8, gear.ElementAt(232)); //Adamant Hat
            this.gear.Add(0x1123, gear.ElementAt(291)); //Metal Jerkin
            this.gear.Add(0x1124, gear.ElementAt(292)); //Adamant Vest
            this.gear.Add(0x10E9, gear.ElementAt(233)); //Officer's Hat
            this.gear.Add(0x10EA, gear.ElementAt(234)); //Chakra Band
            this.gear.Add(0x1125, gear.ElementAt(293)); //Barrel Coat
            this.gear.Add(0x1126, gear.ElementAt(294)); //Power Vest
            this.gear.Add(0x10EB, gear.ElementAt(235)); //Thief's Cap
            this.gear.Add(0x10EC, gear.ElementAt(236)); //Gigas Hat
            this.gear.Add(0x1127, gear.ElementAt(295)); //Ninja Gear
            this.gear.Add(0x1128, gear.ElementAt(296)); //Gigas Chestplate
            this.gear.Add(0x10ED, gear.ElementAt(237)); //Chaperon
            this.gear.Add(0x1129, gear.ElementAt(297)); //Minerva Bustier
            this.gear.Add(0x10EE, gear.ElementAt(238)); //Crown of Laurels
            this.gear.Add(0x112A, gear.ElementAt(298)); //Rubber Suit
            this.gear.Add(0x10EF, gear.ElementAt(239)); //Renewing Morion
            this.gear.Add(0x112B, gear.ElementAt(299)); //Mirage Vest
            this.gear.Add(0x10F1, gear.ElementAt(241)); //Cotton Cap
            this.gear.Add(0x10F2, gear.ElementAt(242)); //Magick Curch
            this.gear.Add(0x112D, gear.ElementAt(301)); //Cotton Shirt
            this.gear.Add(0x112E, gear.ElementAt(302)); //Light Woven Shirt
            this.gear.Add(0x10F3, gear.ElementAt(243)); //Pointy Hat
            this.gear.Add(0x10F4, gear.ElementAt(244)); //Topkapi Hat
            this.gear.Add(0x112F, gear.ElementAt(303)); //Silken Shirt
            this.gear.Add(0x1130, gear.ElementAt(304)); //Kilimweave Shirt
            this.gear.Add(0x10F5, gear.ElementAt(245)); //Calot Hat
            this.gear.Add(0x10F6, gear.ElementAt(246)); //Wizard's Hat
            this.gear.Add(0x1131, gear.ElementAt(305)); //Shepherd's Bolero
            this.gear.Add(0x1132, gear.ElementAt(306)); //Wizard's Robes
            this.gear.Add(0x10F7, gear.ElementAt(247)); //Lambent Hat
            this.gear.Add(0x10F8, gear.ElementAt(248)); //Feathered Cap
            this.gear.Add(0x1133, gear.ElementAt(307)); //Chanter's Djellaba
            this.gear.Add(0x1134, gear.ElementAt(308)); //Traveler's Vestment
            this.gear.Add(0x10F9, gear.ElementAt(249)); //Mage's Hat
            this.gear.Add(0x10FA, gear.ElementAt(250)); //Lamia's Tiara
            this.gear.Add(0x1135, gear.ElementAt(309)); //Mage's Habit
            this.gear.Add(0x1136, gear.ElementAt(310)); //Enchanter's Habit
            this.gear.Add(0x10FB, gear.ElementAt(251)); //Sorcerer's Hat
            this.gear.Add(0x10FC, gear.ElementAt(252)); //Black Cowl
            this.gear.Add(0x1137, gear.ElementAt(311)); //Sorcerer's Habit
            this.gear.Add(0x1138, gear.ElementAt(312)); //Black Garb
            this.gear.Add(0x10FD, gear.ElementAt(253)); //Astrakhan Hat
            this.gear.Add(0x10FE, gear.ElementAt(254)); //Gaia Hat
            this.gear.Add(0x1139, gear.ElementAt(313)); //Carmagnole
            this.gear.Add(0x113A, gear.ElementAt(314)); //Maduin Gear
            this.gear.Add(0x10FF, gear.ElementAt(255)); //Hypnocrown
            this.gear.Add(0x1100, gear.ElementAt(256)); //Gold Hairpin
            this.gear.Add(0x113B, gear.ElementAt(315)); //Jade Gown
            this.gear.Add(0x113C, gear.ElementAt(316)); //Gaia Gear
            this.gear.Add(0x1101, gear.ElementAt(257)); //Celebrant's Miter
            this.gear.Add(0x113D, gear.ElementAt(317)); //Cleric's Robes
            this.gear.Add(0x1102, gear.ElementAt(258)); //Black Mask
            this.gear.Add(0x113F, gear.ElementAt(319)); //Black Robes
            this.gear.Add(0x1103, gear.ElementAt(259)); //White Mask
            this.gear.Add(0x113E, gear.ElementAt(318)); //White Robes
            this.gear.Add(0x1104, gear.ElementAt(260)); //Golden Skullcap
            this.gear.Add(0x1140, gear.ElementAt(320)); //Glimmering Robes
            this.gear.Add(0x115B, gear.ElementAt(347)); //Orrachea Armlet
            this.gear.Add(0x1171, gear.ElementAt(346)); //Bangle
            this.gear.Add(0x115A, gear.ElementAt(369)); //Firefly
            this.gear.Add(0x115E, gear.ElementAt(350)); //Diamond Armlet
            this.gear.Add(0x115D, gear.ElementAt(349)); //Argyle Armlet
            this.gear.Add(0x1177, gear.ElementAt(375)); //Battle Harness
            this.gear.Add(0x116B, gear.ElementAt(363)); //Steel Gorget
            this.gear.Add(0x1168, gear.ElementAt(342)); //Tourmaline Ring
            this.gear.Add(0x1156, gear.ElementAt(360)); //Embroidered Tippet
            this.gear.Add(0x116F, gear.ElementAt(367)); //Golden Amulet
            this.gear.Add(0x1169, gear.ElementAt(361)); //Leather Gorget
            this.gear.Add(0x1166, gear.ElementAt(364)); //Rose Corsage
            this.gear.Add(0x116C, gear.ElementAt(358)); //Turtleshell Choker
            this.gear.Add(0x1162, gear.ElementAt(354)); //Thief's Cuffs
            this.gear.Add(0x1165, gear.ElementAt(357)); //Gauntlets
            this.gear.Add(0x115F, gear.ElementAt(351)); //Amber Armlet
            this.gear.Add(0x1176, gear.ElementAt(374)); //Black Belt
            this.gear.Add(0x116A, gear.ElementAt(362)); //Jade Collar
            this.gear.Add(0x1175, gear.ElementAt(373)); //Nishijin Belt
            this.gear.Add(0x116D, gear.ElementAt(365)); //Pheasant Netsuke
            this.gear.Add(0x1163, gear.ElementAt(355)); //Blazer Gloves
            this.gear.Add(0x117A, gear.ElementAt(378)); //Gillie Boots
            this.gear.Add(0x117B, gear.ElementAt(379)); //Steel Poleyns
            this.gear.Add(0x1160, gear.ElementAt(352)); //Berserker Bracers
            this.gear.Add(0x1161, gear.ElementAt(353)); //Magick Gloves
            this.gear.Add(0x1157, gear.ElementAt(343)); //Sage's Ring
            this.gear.Add(0x1159, gear.ElementAt(345)); //Agate Ring
            this.gear.Add(0x1155, gear.ElementAt(341)); //Ruby Ring
            this.gear.Add(0x1170, gear.ElementAt(368)); //Bowline Sash
            this.gear.Add(0x1174, gear.ElementAt(372)); //Cameo Belt
            this.gear.Add(0x117F, gear.ElementAt(383)); //Cat-ear Hood
            this.gear.Add(0x1173, gear.ElementAt(371)); //Bubble Belt
            this.gear.Add(0x1180, gear.ElementAt(384)); //Fuzzy Miter
            this.gear.Add(0x1172, gear.ElementAt(370)); //Sash
            this.gear.Add(0x115C, gear.ElementAt(348)); //Power Armlet
            this.gear.Add(0x116E, gear.ElementAt(366)); //Indigo Pendant
            this.gear.Add(0x117C, gear.ElementAt(380)); //Winged Boots
            this.gear.Add(0x1154, gear.ElementAt(340)); //Opal Ring
            this.gear.Add(0x1179, gear.ElementAt(377)); //Hermes Sandals
            this.gear.Add(0x1181, gear.ElementAt(385)); //Ribbon
            this.gear.Add(0x100F, gear.ElementAt(15)); //Deathbringer
            this.gear.Add(0x1010, gear.ElementAt(16)); //Stoneblade
            this.gear.Add(0x1011, gear.ElementAt(17)); //Durandal
            this.gear.Add(0x10C2, gear.ElementAt(194)); //Simha
            this.gear.Add(0x10C0, gear.ElementAt(192)); //Karkata
            this.gear.Add(0x1015, gear.ElementAt(21)); //Ragnarok
            this.gear.Add(0x10C1, gear.ElementAt(193)); //Excalipur
            this.gear.Add(0x1021, gear.ElementAt(33)); //Muramasa
            this.gear.Add(0x10AA, gear.ElementAt(170)); //Kumbha
            this.gear.Add(0x1026, gear.ElementAt(38)); //Koga Blade
            this.gear.Add(0x10AE, gear.ElementAt(174)); //Vrsabha
            this.gear.Add(0x103F, gear.ElementAt(63)); //Sweep
            this.gear.Add(0x1040, gear.ElementAt(64)); //Eight-fluted Pole
            this.gear.Add(0x10C6, gear.ElementAt(198)); //Kanya
            this.gear.Add(0x104D, gear.ElementAt(77)); //Perseus Bow
            this.gear.Add(0x104E, gear.ElementAt(78)); //Artemis Bow
            this.gear.Add(0x10C7, gear.ElementAt(199)); //Dhanusha
            this.gear.Add(0x10C5, gear.ElementAt(197)); //Mithuna
            this.gear.Add(0x10C4, gear.ElementAt(196)); //Vrscika
            this.gear.Add(0x1077, gear.ElementAt(119)); //Zwill Crossblade
            this.gear.Add(0x1088, gear.ElementAt(136)); //Judicer's Staff
            this.gear.Add(0x1089, gear.ElementAt(137)); //Cloud Staff
            this.gear.Add(0x1099, gear.ElementAt(153)); //Euclid's Sextant
            this.gear.Add(0x10C3, gear.ElementAt(195)); //Makara
            this.gear.Add(0x10D9, gear.ElementAt(217)); //Venetian Shield
            this.gear.Add(0x1116, gear.ElementAt(277)); //Magepower Shishak
            this.gear.Add(0x1152, gear.ElementAt(338)); //Maximillian
            this.gear.Add(0x1117, gear.ElementAt(279)); //Grand Helm
            this.gear.Add(0x1153, gear.ElementAt(339)); //Grand Armor
            this.gear.Add(0x10F0, gear.ElementAt(240)); //Dueling Mask
            this.gear.Add(0x112C, gear.ElementAt(300)); //Brave Suit
            this.gear.Add(0x1105, gear.ElementAt(261)); //Circlet
            this.gear.Add(0x1141, gear.ElementAt(321)); //Lordly Robes
            this.gear.Add(0x117D, gear.ElementAt(381)); //Quasimodo Boots
            this.gear.Add(0x1167, gear.ElementAt(359)); //Nihopalaoa
            this.gear.Add(0x1178, gear.ElementAt(376)); //Germinas Boots
            this.gear.Add(0x1158, gear.ElementAt(344)); //Ring of Renewal
        }

        void SetupAbilities(IEnumerable<string> abilities)
        {
            this.abilities = new Dictionary<ushort, string>();
            this.abilities.Add(0x0000, abilities.ElementAt(0)); //Cure
            this.abilities.Add(0x0001, abilities.ElementAt(1)); //Blindna
            this.abilities.Add(0x0002, abilities.ElementAt(2)); //Vox
            this.abilities.Add(0x0003, abilities.ElementAt(3)); //Poisona
            this.abilities.Add(0x0033, abilities.ElementAt(51)); //Protect
            this.abilities.Add(0x0034, abilities.ElementAt(52)); //Shell
            this.abilities.Add(0x0004, abilities.ElementAt(4)); //Cura
            this.abilities.Add(0x0005, abilities.ElementAt(5)); //Raise
            this.abilities.Add(0x000C, abilities.ElementAt(12)); //Dispel
            this.abilities.Add(0x0007, abilities.ElementAt(7)); //Stona
            this.abilities.Add(0x0006, abilities.ElementAt(6)); //Curaga
            this.abilities.Add(0x0008, abilities.ElementAt(8)); //Regen
            this.abilities.Add(0x0009, abilities.ElementAt(9)); //Cleanse
            this.abilities.Add(0x000A, abilities.ElementAt(10)); //Esuna
            this.abilities.Add(0x0045, abilities.ElementAt(69)); //Confuse
            this.abilities.Add(0x0036, abilities.ElementAt(54)); //Faith
            this.abilities.Add(0x0012, abilities.ElementAt(18)); //Fire
            this.abilities.Add(0x0013, abilities.ElementAt(19)); //Thunder
            this.abilities.Add(0x0014, abilities.ElementAt(20)); //Blizzard
            this.abilities.Add(0x0039, abilities.ElementAt(57)); //Blind
            this.abilities.Add(0x0015, abilities.ElementAt(21)); //Aqua
            this.abilities.Add(0x003C, abilities.ElementAt(60)); //Silence
            this.abilities.Add(0x0016, abilities.ElementAt(22)); //Aero
            this.abilities.Add(0x003D, abilities.ElementAt(61)); //Sleep
            this.abilities.Add(0x0017, abilities.ElementAt(23)); //Fira
            this.abilities.Add(0x003B, abilities.ElementAt(59)); //Poison
            this.abilities.Add(0x0018, abilities.ElementAt(24)); //Thundara
            this.abilities.Add(0x0019, abilities.ElementAt(25)); //Blizzara
            this.abilities.Add(0x001A, abilities.ElementAt(26)); //Bio
            this.abilities.Add(0x003E, abilities.ElementAt(62)); //Blindga
            this.abilities.Add(0x001B, abilities.ElementAt(27)); //Aeroga
            this.abilities.Add(0x0040, abilities.ElementAt(64)); //Silencega
            this.abilities.Add(0x0027, abilities.ElementAt(39)); //Slow
            this.abilities.Add(0x0028, abilities.ElementAt(40)); //Immobilize
            this.abilities.Add(0x002F, abilities.ElementAt(47)); //Reflect
            this.abilities.Add(0x0029, abilities.ElementAt(41)); //Disable
            this.abilities.Add(0x0047, abilities.ElementAt(71)); //Vanish
            this.abilities.Add(0x0031, abilities.ElementAt(49)); //Balance
            this.abilities.Add(0x004F, abilities.ElementAt(79)); //Gravity
            this.abilities.Add(0x0024, abilities.ElementAt(36)); //Haste
            this.abilities.Add(0x002C, abilities.ElementAt(44)); //Stop
            this.abilities.Add(0x002A, abilities.ElementAt(42)); //Bleed
            this.abilities.Add(0x002B, abilities.ElementAt(43)); //Break
            this.abilities.Add(0x002E, abilities.ElementAt(46)); //Countdown
            this.abilities.Add(0x0025, abilities.ElementAt(37)); //Float
            this.abilities.Add(0x0043, abilities.ElementAt(67)); //Berserk
            this.abilities.Add(0x0046, abilities.ElementAt(70)); //Decoy
            this.abilities.Add(0x003A, abilities.ElementAt(58)); //Oil
            this.abilities.Add(0x0049, abilities.ElementAt(73)); //Drain
            this.abilities.Add(0x0042, abilities.ElementAt(66)); //Reverse
            this.abilities.Add(0x004B, abilities.ElementAt(75)); //Bubble
            this.abilities.Add(0x004A, abilities.ElementAt(74)); //Syphon
            this.abilities.Add(0x000D, abilities.ElementAt(13)); //Dispelga
            this.abilities.Add(0x000F, abilities.ElementAt(15)); //Arise
            this.abilities.Add(0x0011, abilities.ElementAt(17)); //Holy
            this.abilities.Add(0x0010, abilities.ElementAt(16)); //Esunaga
            this.abilities.Add(0x0037, abilities.ElementAt(55)); //Protectga
            this.abilities.Add(0x0038, abilities.ElementAt(56)); //Shellga
            this.abilities.Add(0x000E, abilities.ElementAt(14)); //Renew
            this.abilities.Add(0x004C, abilities.ElementAt(76)); //Dark
            this.abilities.Add(0x004D, abilities.ElementAt(77)); //Darkra
            this.abilities.Add(0x0044, abilities.ElementAt(68)); //Death
            this.abilities.Add(0x004E, abilities.ElementAt(78)); //Darkga
            this.abilities.Add(0x0022, abilities.ElementAt(34)); //Ardor
            this.abilities.Add(0x001F, abilities.ElementAt(63)); //Toxify
            this.abilities.Add(0x003F, abilities.ElementAt(31)); //Shock
            this.abilities.Add(0x0020, abilities.ElementAt(32)); //Scourge
            this.abilities.Add(0x0021, abilities.ElementAt(33)); //Flare
            this.abilities.Add(0x0023, abilities.ElementAt(35)); //Scathe
            this.abilities.Add(0x0030, abilities.ElementAt(48)); //Reflectga
            this.abilities.Add(0x002D, abilities.ElementAt(45)); //Slowga
            this.abilities.Add(0x0035, abilities.ElementAt(53)); //Bravery
            this.abilities.Add(0x000B, abilities.ElementAt(11)); //Curaja
            this.abilities.Add(0x001C, abilities.ElementAt(28)); //Firaga
            this.abilities.Add(0x001D, abilities.ElementAt(29)); //Thundaga
            this.abilities.Add(0x0048, abilities.ElementAt(72)); //Vanishga
            this.abilities.Add(0x0032, abilities.ElementAt(49)); //Balance
            this.abilities.Add(0x001E, abilities.ElementAt(30)); //Blizzaga
            this.abilities.Add(0x0041, abilities.ElementAt(65)); //Sleepga
            this.abilities.Add(0x0050, abilities.ElementAt(80)); //Graviga
            this.abilities.Add(0x0026, abilities.ElementAt(38)); //Hastega
            this.abilities.Add(0x00A9, abilities.ElementAt(169)); //Steal
            this.abilities.Add(0x00B1, abilities.ElementAt(177)); //Libra
            this.abilities.Add(0x009E, abilities.ElementAt(158)); //First Aid
            this.abilities.Add(0x00B2, abilities.ElementAt(178)); //Poach
            this.abilities.Add(0x00A3, abilities.ElementAt(163)); //Charge
            this.abilities.Add(0x00A0, abilities.ElementAt(160)); //Horology
            this.abilities.Add(0x00A5, abilities.ElementAt(165)); //Souleater
            this.abilities.Add(0x00B4, abilities.ElementAt(180)); //Traveler
            this.abilities.Add(0x00B0, abilities.ElementAt(176)); //Numerology
            this.abilities.Add(0x00AC, abilities.ElementAt(172)); //Shear
            this.abilities.Add(0x00A2, abilities.ElementAt(162)); //Achilles
            this.abilities.Add(0x00B5, abilities.ElementAt(181)); //Gil Toss
            this.abilities.Add(0x00AD, abilities.ElementAt(173)); //Charm
            this.abilities.Add(0x00AF, abilities.ElementAt(175)); //Sight Unseeing
            this.abilities.Add(0x00A4, abilities.ElementAt(164)); //Infuse
            this.abilities.Add(0x00A7, abilities.ElementAt(167)); //Addle
            this.abilities.Add(0x00A8, abilities.ElementAt(168)); //Bonecrusher
            this.abilities.Add(0x009F, abilities.ElementAt(159)); //Shades of Black
            this.abilities.Add(0x00A1, abilities.ElementAt(161)); //Stamp
            this.abilities.Add(0x00AB, abilities.ElementAt(171)); //Expose
            this.abilities.Add(0x00AE, abilities.ElementAt(174)); //Revive
            this.abilities.Add(0x00B3, abilities.ElementAt(179)); //1000 Needles
            this.abilities.Add(0x00A6, abilities.ElementAt(166)); //Wither
            this.abilities.Add(0x00AA, abilities.ElementAt(170)); //Telekenesis
        }

        void SetupAugs(IEnumerable<string> augments)
        {
            essentials = string.Join(" ", augments.ElementAt(114).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)); //Essentials
            augs = new Dictionary<ushort, string>();
            augs.Add(5, augments.ElementAt(5));
            for(ushort i = 8; i < 13; i++) augs.Add(i, augments.ElementAt(i));
            for (ushort i = 19; i < 25; i++) augs.Add(i, augments.ElementAt(i));
            augs.Add(26, augments.ElementAt(26));
            augs.Add(30, augments.ElementAt(30));
            augs.Add(35, augments.ElementAt(35));
            for (ushort i = 42; i < 48; i++) augs.Add(i, augments.ElementAt(i));
            for (ushort i = 60; i < 115; i++) augs.Add(i, augments.ElementAt(i));
            for (ushort i = 116; i < 129; i++) augs.Add(i, augments.ElementAt(i));
        }

        void SetupLicenceNames(IEnumerable<string> names)
        {
            this.names = new List<string>();
            foreach(var name in names)
                this.names.Add(name);
            summons = new Dictionary<ushort, string>();
            for (ushort k = 0; k < 13; k++)
                summons.Add((ushort)(0x106 + k), names.ElementAt(k + 18));
        }

        public string GetLicenceName(ushort id)
        {
            return names[id > 0x1800 ? id - 0x1800 : id];
        }

        public string GetComponentString(byte group, byte supgroup, ushort id)
        {
            if (id == 0x0072)
                return essentials;
            if (group >= 0x80 && group <= 0x83) //is gear
                return gear[id];
            if(group == 0x84 || group == 0x85) //is ability
                return abilities[id];
            if (group == 0x88 || group == 0x89) //augment/gambit
                return augs[id];
            if (group == 0x87) //summon
                return summons[id];

            throw new NotImplementedException();
        }
    }
}
