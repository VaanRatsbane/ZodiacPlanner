﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZodiacPlanner
{
    public class LicenceCollection
    {
        protected Dictionary<string, Licence> licences; //pairs to licence
        public bool HasInserts { get { return licences.Values.Any(c => c.inserted); } }

        protected void Init() => licences = new Dictionary<string, Licence>();

        public IEnumerable<Licence> Search(string query)
        {
            if (query.Length == 0)
                return licences.Values.AsEnumerable();
            else
                return licences.Values.Where(o => o.Query(query));
        }

        public Licence GetLicence(string pairs)
        {
            return licences[pairs];
        }

        public Licence GetLicence(int index)
        {
            return licences.Values.ElementAt(index);
        }

        public void Clear()
        {
            foreach (var l in licences)
                l.Value.Clear();
        }
    }

    class DefaultLicenceCollection : LicenceCollection
    {
        const string DATA = "licenceinfo.txt";

        public DefaultLicenceCollection()
        {
            Init();
            Licence.SetDictionary(new string[]
            {
                "Quickenings", "Summon", "Essentials",
                "Swords", "Greatswords", "Katanas",
                "Ninja Swords", "Spears", "Poles",
                "Bows", "Crossbows", "Guns",
                "Axes & Hammers", "Daggers", "Rods",
                "Staves", "Maces", "Measures",
                "Hand Bombs", "Shields", "Heavy Armor",
                "Light Armor", "Mystic Armor", "Accessories",
                "White Magicks", "Black Magicks", "Time Magicks",
                "Green Magicks", "Arcane Magicks", "Augments",
                "Gambit Slots", "Technicks", "Second Board"
            });
            var data = File.ReadAllLines(DATA);
            foreach (var line in data)
            {
                var bits = line.Split(',');

                string licenceTypeStr = bits[0];
                LicenceType licenceType;
                if (int.TryParse(licenceTypeStr, out int licenceTypeInt))
                    licenceType = (LicenceType)licenceTypeInt;
                else
                    licenceType = (LicenceType)(9 + licenceTypeStr[0] - 64);

                string pair1 = bits[1];
                string pair2 = bits[2];
                string name = bits[3];
                int lpCost = int.Parse(bits[4]);
                var list = new List<string>();
                for (int i = 5; i < bits.Length; i++)
                    list.Add(bits[i]);
                licences.Add($"{pair1}{pair2}", new Licence(licenceType, pair1, pair2, lpCost, name, list.ToArray()));
            }
        }
    }
}
