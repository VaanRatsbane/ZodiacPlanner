﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZodiacPlanner
{
    public class BattlePackParser
    {
        //TODO decoders for each language
        public static Dictionary<byte, char> GetDecoder(string lang)
        {
            var lines = File.ReadAllLines(lang + "-encoding.csv", Encoding.GetEncoding("iso-8859-1"));
            var dict = new Dictionary<byte, char>();

            foreach (var line in lines)
            {
                var frags = line.Split(',');
                char value;
                switch (frags[0])
                {
                    case "COMMA": value = ','; break;
                    case "\u008c": value = 'Œ'; break;
                    case "\u009c": value = 'œ'; break;
                    default: value = frags[0][0]; break;
                }
                byte key = CallTryParse(frags[1], NumberStyles.HexNumber);
                dict.Add(key, value);
            }

            return dict;
        }

        public static byte[] GetBPackFromVbf(string vbfpath, string language)
        {
            var reader = new VirtuosBigFileReader();
            reader.LoadBigFileFile(vbfpath);
            return reader.ExtractFileContents("ps2data/image/ff12/test_battle/" + language + "/binaryfile/battle_pack.bin").ToArray();
        }

        public static LicenceCollection GetLicenceCollection(string language, byte[] bpack)
        {
            var battlepack = new BattlePack(bpack);
            var wordbin = new WordPack(battlepack.GetSubFile(38));
            var decoder = GetDecoder(language);

            Func<string, string> treatString = (string original) =>
            {
                return original.Substring(8, original.IndexOf('{', 8) - 8);
            };

            var abilities = new WordFile(wordbin.GetSubFile(0)).Decode(decoder).Select(c => treatString(c));
            var gear = new WordFile(wordbin.GetSubFile(1)).Decode(decoder).Select(c => treatString(c));
            var licenceNames = new WordFile(wordbin.GetSubFile(3)).Decode(decoder).Select(c => treatString(c));
            var augments = new WordFile(wordbin.GetSubFile(10)).Decode(decoder).Select(c => treatString(c));

            var loadedStrings = new LoadedStrings(licenceNames, gear, abilities, augments);

            int start;
            switch(language)
            {
                case "ch": start = 0x000089B8; break;
    	        case "cn": start = 0x000089C0; break;
                case "de": start = 0x000091B8; break;
                case "es": start = 0x00009110; break;
                case "fr": start = 0x00008E58; break;
                case "it": start = 0x00008ED8; break;
                case "kr": start = 0x00008E78; break;
                case "us": start = 0x00008E68; break;
                case "in": default: start = 0x000089A8; break;
            }

            var col = new LoadedLicenceCollection();
            byte quickeningCounter = 0;
            for(int l = 0; l < 361; l++)
            {
                byte[] data = new byte[24];
                Array.Copy(bpack, start + 24 * l, data, 0, 24);
                if (data[0] == 0x69 && data[1] == 0x19)
                    col.AddQuickening(new Licence(loadedStrings, quickeningCounter++, data[4]));
                else
                    col.AddLicence(new Licence(data, loadedStrings));
            }
            return col;
        }

        internal class LoadedLicenceCollection : LicenceCollection
        {
            public LoadedLicenceCollection()
            {
                Init();
            }

            public void AddLicence(Licence value)
            {
                licences.Add($"{value.pair1}{((byte)(Convert.ToByte(value.pair2, 16))).ToString("X2")}", value);
            }

            public void AddQuickening(Licence value)
            {
                licences.Add($"{value.pair1}{Convert.ToByte(value.pair2, 16).ToString("X2")}", value);
            }
        }

        private static byte CallTryParse(string stringToConvert, NumberStyles styles = NumberStyles.HexNumber)
        {
            Byte byteValue;
            bool result = Byte.TryParse(stringToConvert, styles,
                                        null as IFormatProvider, out byteValue);
            return byteValue;
        }
    }
}
