﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZodiacPlanner
{
    public class ColorManager
    {
        Dictionary<LicenceType, Color> colors;

        const string COLORFILE = "colors.json";

        public ColorManager()
        {
            try
            {
                var json = File.ReadAllText(COLORFILE);
                colors = JsonConvert.DeserializeObject<Dictionary<LicenceType, Color>>(json);
            }
            catch
            {
                Initialize();
            }
        }

        public void Save()
        {
            try
            {
                var json = JsonConvert.SerializeObject(colors, Formatting.Indented);
                File.WriteAllText(COLORFILE, json);
            }
            catch (Exception e)
            {
                MessageBox.Show($"Error saving the color settings.\n{e.ToString()}");
            }
        }

        public Color Get(LicenceType t)
        {
            return colors[t];
        }

        public void Set(LicenceType c, Color color)
        {
            colors[c] = color;
        }

        public void Initialize()
        {
            colors = new Dictionary<LicenceType, Color>();
            colors[LicenceType.QUICKENING] = System.Drawing.ColorTranslator.FromHtml("#ffff0000");
            colors[LicenceType.SUMMON] = System.Drawing.ColorTranslator.FromHtml("#fff20000");
            colors[LicenceType.ESSENTIALS] = System.Drawing.ColorTranslator.FromHtml("#ff400000");
            colors[LicenceType.SWORDS] = System.Drawing.ColorTranslator.FromHtml("#ffff9180");
            colors[LicenceType.GSWORDS] = System.Drawing.ColorTranslator.FromHtml("#ff241a33");
            colors[LicenceType.KATANAS] = System.Drawing.ColorTranslator.FromHtml("#ff664133");
            colors[LicenceType.NINJA] = System.Drawing.ColorTranslator.FromHtml("#ffa64200");
            colors[LicenceType.SPEARS] = System.Drawing.ColorTranslator.FromHtml("#ffa68d7c");
            colors[LicenceType.POLES] = System.Drawing.ColorTranslator.FromHtml("#ffff8800");
            colors[LicenceType.BOWS] = System.Drawing.ColorTranslator.FromHtml("#ffffc480");
            colors[LicenceType.CROSSBOWS] = System.Drawing.ColorTranslator.FromHtml("#ff8c5e00");
            colors[LicenceType.GUNS] = System.Drawing.ColorTranslator.FromHtml("#ff332200");
            colors[LicenceType.AXES] = System.Drawing.ColorTranslator.FromHtml("#ffffcc00");
            colors[LicenceType.DAGGERS] = System.Drawing.ColorTranslator.FromHtml("#fffff2bf");
            colors[LicenceType.RODS] = System.Drawing.ColorTranslator.FromHtml("#ff403d30");
            colors[LicenceType.STAVES] = System.Drawing.ColorTranslator.FromHtml("#ff595300");
            colors[LicenceType.MACES] = System.Drawing.ColorTranslator.FromHtml("#ffeeff00");
            colors[LicenceType.MEASURES] = System.Drawing.ColorTranslator.FromHtml("#ff8da629");
            colors[LicenceType.BOMBS] = System.Drawing.ColorTranslator.FromHtml("#ff858c69");
            colors[LicenceType.SHIELDS] = System.Drawing.ColorTranslator.FromHtml("#ff88ff00");
            colors[LicenceType.HEAVY] = System.Drawing.ColorTranslator.FromHtml("#ffd9ffbf");
            colors[LicenceType.LIGHT] = System.Drawing.ColorTranslator.FromHtml("#ff285916");
            colors[LicenceType.MYSTIC] = System.Drawing.ColorTranslator.FromHtml("#ff149900");
            colors[LicenceType.ACCESSORIES] = System.Drawing.ColorTranslator.FromHtml("#ff00ff88");
            colors[LicenceType.WHITE] = System.Drawing.ColorTranslator.FromHtml("#ff00a66f");
            colors[LicenceType.BLACK] = System.Drawing.ColorTranslator.FromHtml("#ff005947");
            colors[LicenceType.TIME] = System.Drawing.ColorTranslator.FromHtml("#ff00ffee");
            colors[LicenceType.GREEN] = System.Drawing.ColorTranslator.FromHtml("#ff8fbfbc");
            colors[LicenceType.ARCANE] = System.Drawing.ColorTranslator.FromHtml("#ff00ccff");
            colors[LicenceType.AUG] = System.Drawing.ColorTranslator.FromHtml("#ff1d6273");
            colors[LicenceType.GAMBIT] = System.Drawing.ColorTranslator.FromHtml("#ff0077b3");
            colors[LicenceType.TECH] = System.Drawing.ColorTranslator.FromHtml("#ff0088ff");
            colors[LicenceType.SECONDBOARD] = System.Drawing.ColorTranslator.FromHtml("#ff13324d");
        }
    }

}
