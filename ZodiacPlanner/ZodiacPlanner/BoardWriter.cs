﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZodiacPlanner
{
    class BoardWriter
    {

        static string[] prefix = new string[] { "6C", "69", "63", "64", "18", "00", "18", "00" };
        public static string filePath;

        public static string[] Load(string filePath)
        {
            FileStream fs = null;
            try
            {
                if (new System.IO.FileInfo(filePath).Length != 1160)
                    return null;
                fs = new FileStream(filePath, FileMode.Open);
                int hexIn;
                var list = new List<string>();
                for (int i = 0; (hexIn = fs.ReadByte()) != -1; i++)
                    if(i>=8)
                        list.Add(string.Format("{0:X2}", hexIn));

                if (list.Count != (24 * 24 * 2))
                    return null;

                BoardWriter.filePath = filePath;
                return list.ToArray();
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if(fs != null)
                    fs.Close();
            }
        }

        public static void Write(TableLayoutPanel table)
        {
            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                using (BinaryWriter r = new BinaryWriter(fs))
                {
                    foreach(var s in prefix)
                        r.Write(Convert.ToByte(s, 16));

                    for (int y = 1; y < 25; y++)
                        for (int x = 1; x < 25; x++)
                        {
                            var control = table.GetControlFromPosition(x, y);
                            if (control.Tag == null)
                            {
                                r.Write(Convert.ToByte("FF", 16));
                                r.Write(Convert.ToByte("FF", 16));
                            }
                            else
                            {
                                var btn = control as Button;
                                var l = btn.Tag as Licence;
                                var binaryval = Convert.ToByte(l.pair1, 16);
                                var binaryval2 = Convert.ToByte(l.pair2, 16);
                                r.Write(binaryval);
                                r.Write(binaryval2);
                            }
                        }
                    r.Flush();
                }
            }

            
        }

        public static string[] Rotate(TableLayoutPanel table, bool right)
        {
            var current = new byte[24 * 24 * 2];

            //Write
            {
                int i = 0;
                for (int y = 1; y < 25; y++)
                    for (int x = 1; x < 25; x++)
                    {
                        var control = table.GetControlFromPosition(x, y);
                        if (control.Tag == null)
                        {
                            current[i++] = Convert.ToByte("FF", 16);
                            current[i++] = Convert.ToByte("FF", 16);
                        }
                        else
                        {
                            var btn = control as Button;
                            var l = btn.Tag as Licence;
                            var binaryval = Convert.ToByte(l.pair1, 16);
                            var binaryval2 = Convert.ToByte(l.pair2, 16);
                            current[i++] = binaryval;
                            current[i++] = binaryval2;
                        }
                    }
            }

            return right ? RotateRight(current) : RotateLeft(current);
        }

        private static string[] RotateRight(byte[] current)
        {
            var rotated = new byte[24 * 24 * 2];
            for (int row = 0; row < 24; row++)
                for (int col = 0; col < 24; col++)
                {
                    var oldPos = (row * 24 + col) * 2;
                    var newPos = (col * 24 + 23 - row) * 2;

                    rotated[newPos] = current[oldPos];
                    rotated[newPos + 1] = current[oldPos + 1];
                }
            return rotated.Select(c => string.Format("{0:X2}", c)).ToArray();
        }

        private static string[] RotateLeft(byte[] current)
        {
            var rotated = new byte[24 * 24 * 2];
            for (int row = 0; row < 24; row++)
                for (int col = 0; col < 24; col++)
                {
                    var oldPos = (row * 24 + col) * 2;
                    var newPos = ((23 - col) * 24 + row) * 2;

                    rotated[newPos] = current[oldPos];
                    rotated[newPos + 1] = current[oldPos + 1];
                }
            return rotated.Select(c => string.Format("{0:X2}", c)).ToArray();
        }

        public static string[] Flip(TableLayoutPanel table, bool horiz)
        {
            var current = new byte[24 * 24 * 2];

            //Write
            {
                int i = 0;
                for (int y = 1; y < 25; y++)
                    for (int x = 1; x < 25; x++)
                    {
                        var control = table.GetControlFromPosition(x, y);
                        if (control.Tag == null)
                        {
                            current[i++] = Convert.ToByte("FF", 16);
                            current[i++] = Convert.ToByte("FF", 16);
                        }
                        else
                        {
                            var btn = control as Button;
                            var l = btn.Tag as Licence;
                            var binaryval = Convert.ToByte(l.pair1, 16);
                            var binaryval2 = Convert.ToByte(l.pair2, 16);
                            current[i++] = binaryval;
                            current[i++] = binaryval2;
                        }
                    }
            }

            return horiz ? FlipHorizontal(current) : FlipVertical(current);
        }

        private static string[] FlipHorizontal(byte[] current)
        {
            for (int row = 0; row < 24; row++)
                for (int col = 0; col < 12; col++)
                {
                    var leftPos = (row * 24 + col) * 2;
                    var rightPos = (row * 24 + 23 - col) * 2;
                    var temp = current[leftPos];
                    var temp2 = current[leftPos + 1];
                    current[leftPos] = current[rightPos];
                    current[leftPos + 1] = current[rightPos + 1];
                    current[rightPos] = temp;
                    current[rightPos + 1] = temp2;
                }
            return current.Select(c => string.Format("{0:X2}", c)).ToArray();
        }

        private static string[] FlipVertical(byte[] current)
        {
            for (int col = 0; col < 24; col++)
                for (int row = 0; row < 12; row++)
                {
                    var topPos = (row * 24 + col) * 2;
                    var botPos = ((23 - row) * 24 + col) * 2;
                    var temp = current[topPos];
                    var temp2 = current[topPos + 1];
                    current[topPos] = current[botPos];
                    current[topPos + 1] = current[botPos + 1];
                    current[botPos] = temp;
                    current[botPos + 1] = temp2;
                }
            return current.Select(c => string.Format("{0:X2}", c)).ToArray();
        }

    }
}
