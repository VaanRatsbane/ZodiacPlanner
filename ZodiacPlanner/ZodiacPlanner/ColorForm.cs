﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZodiacPlanner
{
    public partial class ColorForm : Form
    {

        private ColorManager colors;
        private bool hasChanges;

        public ColorForm()
        {
            InitializeComponent();
            this.colors = Program.colors;
            LoadColors();
            hasChanges = false;
        }

        private void LoadColors()
        {
            quickenings.BackColor = colors.Get(LicenceType.QUICKENING);
            summons.BackColor = colors.Get(LicenceType.SUMMON);
            essentials.BackColor = colors.Get(LicenceType.ESSENTIALS);
            swords.BackColor = colors.Get(LicenceType.SWORDS);
            greatswords.BackColor = colors.Get(LicenceType.GSWORDS);
            katanas.BackColor = colors.Get(LicenceType.KATANAS);
            ninjaswords.BackColor = colors.Get(LicenceType.NINJA);
            spears.BackColor = colors.Get(LicenceType.SPEARS);
            poles.BackColor = colors.Get(LicenceType.POLES);
            bows.BackColor = colors.Get(LicenceType.BOWS);
            crossbows.BackColor = colors.Get(LicenceType.CROSSBOWS);
            guns.BackColor = colors.Get(LicenceType.GUNS);
            axes.BackColor = colors.Get(LicenceType.AXES);
            daggers.BackColor = colors.Get(LicenceType.DAGGERS);
            rods.BackColor = colors.Get(LicenceType.RODS);
            staves.BackColor = colors.Get(LicenceType.STAVES);
            maces.BackColor = colors.Get(LicenceType.MACES);
            measures.BackColor = colors.Get(LicenceType.MEASURES);
            handbombs.BackColor = colors.Get(LicenceType.BOMBS);
            shields.BackColor = colors.Get(LicenceType.SHIELDS);
            heavy.BackColor = colors.Get(LicenceType.HEAVY);
            light.BackColor = colors.Get(LicenceType.LIGHT);
            mystic.BackColor = colors.Get(LicenceType.MYSTIC);
            accessories.BackColor = colors.Get(LicenceType.ACCESSORIES);
            whm.BackColor = colors.Get(LicenceType.WHITE);
            blm.BackColor = colors.Get(LicenceType.BLACK);
            time.BackColor = colors.Get(LicenceType.TIME);
            green.BackColor = colors.Get(LicenceType.GREEN);
            arcane.BackColor = colors.Get(LicenceType.ARCANE);
            augments.BackColor = colors.Get(LicenceType.AUG);
            gambits.BackColor = colors.Get(LicenceType.GAMBIT);
            technicks.BackColor = colors.Get(LicenceType.TECH);
            secondboard.BackColor = colors.Get(LicenceType.SECONDBOARD);
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            if (hasChanges)
            {
                colors.Save();
                (Owner as Form1).redraw = true;
            }
            Close();
        }

        private void changeColor(Control control, LicenceType type)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                control.BackColor = colorDialog1.Color;
                colors.Set(type, colorDialog1.Color);
                hasChanges = true;
            }
        }

        private void quickenings_Click(object sender, EventArgs e)
        {
            changeColor(quickenings, LicenceType.QUICKENING);
        }

        private void summons_Click(object sender, EventArgs e)
        {
            changeColor(summons, LicenceType.SUMMON);
        }

        private void essentials_Click(object sender, EventArgs e)
        {
            changeColor(essentials, LicenceType.ESSENTIALS);
        }

        private void swords_Click(object sender, EventArgs e)
        {
            changeColor(swords, LicenceType.SWORDS);
        }

        private void greatswords_Click(object sender, EventArgs e)
        {
            changeColor(greatswords, LicenceType.GSWORDS);
        }

        private void katanas_Click(object sender, EventArgs e)
        {
            changeColor(katanas, LicenceType.KATANAS);
        }

        private void ninjaswords_Click(object sender, EventArgs e)
        {
            changeColor(ninjaswords, LicenceType.NINJA);
        }

        private void spears_Click(object sender, EventArgs e)
        {
            changeColor(spears, LicenceType.SPEARS);
        }

        private void poles_Click(object sender, EventArgs e)
        {
            changeColor(poles, LicenceType.POLES);
        }

        private void bows_Click(object sender, EventArgs e)
        {
            changeColor(bows, LicenceType.BOWS);
        }

        private void crossbows_Click(object sender, EventArgs e)
        {
            changeColor(crossbows, LicenceType.CROSSBOWS);
        }

        private void guns_Click(object sender, EventArgs e)
        {
            changeColor(guns, LicenceType.GUNS);
        }

        private void axes_Click(object sender, EventArgs e)
        {
            changeColor(axes, LicenceType.AXES);
        }

        private void daggers_Click(object sender, EventArgs e)
        {
            changeColor(daggers, LicenceType.DAGGERS);
        }

        private void rods_Click(object sender, EventArgs e)
        {
            changeColor(rods, LicenceType.RODS);
        }

        private void staves_Click(object sender, EventArgs e)
        {
            changeColor(staves, LicenceType.STAVES);
        }

        private void maces_Click(object sender, EventArgs e)
        {
            changeColor(maces, LicenceType.MACES);
        }

        private void measures_Click(object sender, EventArgs e)
        {
            changeColor(measures, LicenceType.MEASURES);
        }

        private void handbombs_Click(object sender, EventArgs e)
        {
            changeColor(handbombs, LicenceType.BOMBS);
        }

        private void shields_Click(object sender, EventArgs e)
        {
            changeColor(shields, LicenceType.SHIELDS);
        }

        private void heavy_Click(object sender, EventArgs e)
        {
            changeColor(heavy, LicenceType.HEAVY);
        }

        private void light_Click(object sender, EventArgs e)
        {
            changeColor(light, LicenceType.LIGHT);
        }

        private void mystic_Click(object sender, EventArgs e)
        {
            changeColor(mystic, LicenceType.MYSTIC);
        }

        private void accessories_Click(object sender, EventArgs e)
        {
            changeColor(accessories, LicenceType.ACCESSORIES);
        }

        private void whm_Click(object sender, EventArgs e)
        {
            changeColor(whm, LicenceType.WHITE);
        }

        private void blm_Click(object sender, EventArgs e)
        {
            changeColor(blm, LicenceType.BLACK);
        }

        private void time_Click(object sender, EventArgs e)
        {
            changeColor(time, LicenceType.TIME);
        }

        private void green_Click(object sender, EventArgs e)
        {
            changeColor(green, LicenceType.GREEN);
        }

        private void arcane_Click(object sender, EventArgs e)
        {
            changeColor(arcane, LicenceType.ARCANE);
        }

        private void augments_Click(object sender, EventArgs e)
        {
            changeColor(augments, LicenceType.AUG);
        }

        private void gambits_Click(object sender, EventArgs e)
        {
            changeColor(gambits, LicenceType.GAMBIT);
        }

        private void technicks_Click(object sender, EventArgs e)
        {
            changeColor(technicks, LicenceType.TECH);
        }

        private void secondboard_Click(object sender, EventArgs e)
        {
            changeColor(secondboard, LicenceType.SECONDBOARD);
        }

        private void revertDefaults_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Revert colors to their default values?", "Warning", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                colors.Initialize();
                LoadColors();
                hasChanges = true;
            }
        }
    }
}
