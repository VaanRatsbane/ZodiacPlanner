﻿namespace ZodiacPlanner
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.button50 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.outputLogCheck = new System.Windows.Forms.CheckBox();
            this.colorSettings = new System.Windows.Forms.Button();
            this.toggleColor = new System.Windows.Forms.Button();
            this.quitBtn = new System.Windows.Forms.PictureBox();
            this.aboutBtn = new System.Windows.Forms.PictureBox();
            this.saveBtn = new System.Windows.Forms.PictureBox();
            this.loadBtn = new System.Windows.Forms.PictureBox();
            this.newButton = new System.Windows.Forms.PictureBox();
            this.rightClick = new System.Windows.Forms.CheckBox();
            this.doubleClick = new System.Windows.Forms.CheckBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.boardPanel = new System.Windows.Forms.TableLayoutPanel();
            this.button26 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.button48 = new System.Windows.Forms.Button();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.clearButton = new System.Windows.Forms.Button();
            this.selectedLPCost = new System.Windows.Forms.TextBox();
            this.selectedContent4 = new System.Windows.Forms.TextBox();
            this.selectedContent3 = new System.Windows.Forms.TextBox();
            this.selectedContent2 = new System.Windows.Forms.TextBox();
            this.selectedContent1 = new System.Windows.Forms.TextBox();
            this.selectedCellText = new System.Windows.Forms.TextBox();
            this.logLabel = new System.Windows.Forms.Label();
            this.showLogBtn = new System.Windows.Forms.Button();
            this.insertButton = new System.Windows.Forms.Button();
            this.listLicence4 = new System.Windows.Forms.TextBox();
            this.listLicence3 = new System.Windows.Forms.TextBox();
            this.listLicence2 = new System.Windows.Forms.TextBox();
            this.listLicence1 = new System.Windows.Forms.TextBox();
            this.licenceList = new System.Windows.Forms.ListView();
            this.codeHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.licenceHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lpHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.typeHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.insertedPos = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.searchBox = new System.Windows.Forms.TextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button51 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.button53 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quitBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aboutBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.boardPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.button52);
            this.splitContainer1.Panel1.Controls.Add(this.button53);
            this.splitContainer1.Panel1.Controls.Add(this.button51);
            this.splitContainer1.Panel1.Controls.Add(this.button50);
            this.splitContainer1.Panel1.Controls.Add(this.button49);
            this.splitContainer1.Panel1.Controls.Add(this.outputLogCheck);
            this.splitContainer1.Panel1.Controls.Add(this.colorSettings);
            this.splitContainer1.Panel1.Controls.Add(this.toggleColor);
            this.splitContainer1.Panel1.Controls.Add(this.quitBtn);
            this.splitContainer1.Panel1.Controls.Add(this.aboutBtn);
            this.splitContainer1.Panel1.Controls.Add(this.saveBtn);
            this.splitContainer1.Panel1.Controls.Add(this.loadBtn);
            this.splitContainer1.Panel1.Controls.Add(this.newButton);
            this.splitContainer1.Panel1.Controls.Add(this.rightClick);
            this.splitContainer1.Panel1.Controls.Add(this.doubleClick);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1264, 861);
            this.splitContainer1.SplitterDistance = 61;
            this.splitContainer1.TabIndex = 0;
            // 
            // button50
            // 
            this.button50.Location = new System.Drawing.Point(489, 8);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(144, 23);
            this.button50.TabIndex = 11;
            this.button50.Text = "Rotate Right";
            this.button50.UseVisualStyleBackColor = true;
            this.button50.Click += new System.EventHandler(this.button50_Click);
            // 
            // button49
            // 
            this.button49.Location = new System.Drawing.Point(639, 8);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(144, 23);
            this.button49.TabIndex = 10;
            this.button49.Text = "Load Custom Licences";
            this.button49.UseVisualStyleBackColor = true;
            this.button49.Click += new System.EventHandler(this.button49_Click);
            // 
            // outputLogCheck
            // 
            this.outputLogCheck.AutoSize = true;
            this.outputLogCheck.Location = new System.Drawing.Point(792, 12);
            this.outputLogCheck.Name = "outputLogCheck";
            this.outputLogCheck.Size = new System.Drawing.Size(103, 17);
            this.outputLogCheck.TabIndex = 9;
            this.outputLogCheck.Text = "Output log to file";
            this.outputLogCheck.UseVisualStyleBackColor = true;
            this.outputLogCheck.CheckedChanged += new System.EventHandler(this.outputLogCheck_CheckedChanged);
            // 
            // colorSettings
            // 
            this.colorSettings.Location = new System.Drawing.Point(1146, 8);
            this.colorSettings.Name = "colorSettings";
            this.colorSettings.Size = new System.Drawing.Size(106, 23);
            this.colorSettings.TabIndex = 8;
            this.colorSettings.Text = "Color Settings";
            this.colorSettings.UseVisualStyleBackColor = true;
            this.colorSettings.Click += new System.EventHandler(this.colorSettings_Click);
            // 
            // toggleColor
            // 
            this.toggleColor.Location = new System.Drawing.Point(1037, 8);
            this.toggleColor.Name = "toggleColor";
            this.toggleColor.Size = new System.Drawing.Size(106, 23);
            this.toggleColor.TabIndex = 7;
            this.toggleColor.Text = "Toggle Dark Mode";
            this.toggleColor.UseVisualStyleBackColor = true;
            this.toggleColor.Click += new System.EventHandler(this.toggleColor_Click);
            // 
            // quitBtn
            // 
            this.quitBtn.Image = global::ZodiacPlanner.Properties.Resources.quit;
            this.quitBtn.Location = new System.Drawing.Point(224, 3);
            this.quitBtn.Name = "quitBtn";
            this.quitBtn.Size = new System.Drawing.Size(47, 47);
            this.quitBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.quitBtn.TabIndex = 6;
            this.quitBtn.TabStop = false;
            this.quitBtn.Click += new System.EventHandler(this.quitBtn_Click);
            // 
            // aboutBtn
            // 
            this.aboutBtn.Image = global::ZodiacPlanner.Properties.Resources.about;
            this.aboutBtn.Location = new System.Drawing.Point(171, 3);
            this.aboutBtn.Name = "aboutBtn";
            this.aboutBtn.Size = new System.Drawing.Size(47, 47);
            this.aboutBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.aboutBtn.TabIndex = 5;
            this.aboutBtn.TabStop = false;
            this.aboutBtn.Click += new System.EventHandler(this.aboutBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.Image = global::ZodiacPlanner.Properties.Resources.save;
            this.saveBtn.Location = new System.Drawing.Point(118, 3);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(47, 47);
            this.saveBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.saveBtn.TabIndex = 4;
            this.saveBtn.TabStop = false;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // loadBtn
            // 
            this.loadBtn.Image = global::ZodiacPlanner.Properties.Resources.load;
            this.loadBtn.Location = new System.Drawing.Point(65, 3);
            this.loadBtn.Name = "loadBtn";
            this.loadBtn.Size = new System.Drawing.Size(47, 47);
            this.loadBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.loadBtn.TabIndex = 3;
            this.loadBtn.TabStop = false;
            this.loadBtn.Click += new System.EventHandler(this.loadBtn_Click);
            // 
            // newButton
            // 
            this.newButton.Image = global::ZodiacPlanner.Properties.Resources._new;
            this.newButton.Location = new System.Drawing.Point(12, 3);
            this.newButton.Name = "newButton";
            this.newButton.Size = new System.Drawing.Size(47, 47);
            this.newButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.newButton.TabIndex = 2;
            this.newButton.TabStop = false;
            this.newButton.Click += new System.EventHandler(this.newButton_Click);
            // 
            // rightClick
            // 
            this.rightClick.AutoSize = true;
            this.rightClick.Location = new System.Drawing.Point(906, 35);
            this.rightClick.Name = "rightClick";
            this.rightClick.Size = new System.Drawing.Size(126, 17);
            this.rightClick.TabIndex = 1;
            this.rightClick.Text = "Right click to remove";
            this.rightClick.UseVisualStyleBackColor = true;
            this.rightClick.CheckedChanged += new System.EventHandler(this.rightClick_CheckedChanged);
            // 
            // doubleClick
            // 
            this.doubleClick.AutoSize = true;
            this.doubleClick.Location = new System.Drawing.Point(906, 12);
            this.doubleClick.Name = "doubleClick";
            this.doubleClick.Size = new System.Drawing.Size(125, 17);
            this.doubleClick.TabIndex = 0;
            this.doubleClick.Text = "Double click to insert";
            this.doubleClick.UseVisualStyleBackColor = true;
            this.doubleClick.CheckedChanged += new System.EventHandler(this.doubleClick_CheckedChanged);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.AutoScroll = true;
            this.splitContainer2.Panel1.Controls.Add(this.boardPanel);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(1264, 796);
            this.splitContainer2.SplitterDistance = 895;
            this.splitContainer2.TabIndex = 0;
            // 
            // boardPanel
            // 
            this.boardPanel.BackgroundImage = global::ZodiacPlanner.Properties.Resources.bg;
            this.boardPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.boardPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.boardPanel.ColumnCount = 25;
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.4961F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.85492F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.856849F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.531494F));
            this.boardPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.123628F));
            this.boardPanel.Controls.Add(this.button26, 0, 1);
            this.boardPanel.Controls.Add(this.button25, 24, 0);
            this.boardPanel.Controls.Add(this.button24, 23, 0);
            this.boardPanel.Controls.Add(this.button23, 22, 0);
            this.boardPanel.Controls.Add(this.button22, 21, 0);
            this.boardPanel.Controls.Add(this.button21, 20, 0);
            this.boardPanel.Controls.Add(this.button20, 19, 0);
            this.boardPanel.Controls.Add(this.button19, 18, 0);
            this.boardPanel.Controls.Add(this.button18, 17, 0);
            this.boardPanel.Controls.Add(this.button17, 16, 0);
            this.boardPanel.Controls.Add(this.button16, 15, 0);
            this.boardPanel.Controls.Add(this.button15, 14, 0);
            this.boardPanel.Controls.Add(this.button14, 13, 0);
            this.boardPanel.Controls.Add(this.button13, 12, 0);
            this.boardPanel.Controls.Add(this.button12, 11, 0);
            this.boardPanel.Controls.Add(this.button11, 10, 0);
            this.boardPanel.Controls.Add(this.button10, 9, 0);
            this.boardPanel.Controls.Add(this.button9, 8, 0);
            this.boardPanel.Controls.Add(this.button8, 7, 0);
            this.boardPanel.Controls.Add(this.button7, 6, 0);
            this.boardPanel.Controls.Add(this.button6, 5, 0);
            this.boardPanel.Controls.Add(this.button5, 4, 0);
            this.boardPanel.Controls.Add(this.button4, 3, 0);
            this.boardPanel.Controls.Add(this.button3, 2, 0);
            this.boardPanel.Controls.Add(this.button1, 1, 0);
            this.boardPanel.Controls.Add(this.button2, 0, 2);
            this.boardPanel.Controls.Add(this.button27, 0, 3);
            this.boardPanel.Controls.Add(this.button28, 0, 4);
            this.boardPanel.Controls.Add(this.button29, 0, 5);
            this.boardPanel.Controls.Add(this.button30, 0, 6);
            this.boardPanel.Controls.Add(this.button31, 0, 7);
            this.boardPanel.Controls.Add(this.button32, 0, 8);
            this.boardPanel.Controls.Add(this.button33, 0, 9);
            this.boardPanel.Controls.Add(this.button34, 0, 10);
            this.boardPanel.Controls.Add(this.button35, 0, 11);
            this.boardPanel.Controls.Add(this.button36, 0, 12);
            this.boardPanel.Controls.Add(this.button37, 0, 13);
            this.boardPanel.Controls.Add(this.button38, 0, 14);
            this.boardPanel.Controls.Add(this.button39, 0, 15);
            this.boardPanel.Controls.Add(this.button40, 0, 16);
            this.boardPanel.Controls.Add(this.button41, 0, 17);
            this.boardPanel.Controls.Add(this.button42, 0, 18);
            this.boardPanel.Controls.Add(this.button43, 0, 19);
            this.boardPanel.Controls.Add(this.button44, 0, 20);
            this.boardPanel.Controls.Add(this.button45, 0, 21);
            this.boardPanel.Controls.Add(this.button46, 0, 22);
            this.boardPanel.Controls.Add(this.button47, 0, 23);
            this.boardPanel.Controls.Add(this.button48, 0, 24);
            this.boardPanel.Location = new System.Drawing.Point(12, 3);
            this.boardPanel.Margin = new System.Windows.Forms.Padding(0);
            this.boardPanel.Name = "boardPanel";
            this.boardPanel.RowCount = 25;
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.boardPanel.Size = new System.Drawing.Size(883, 784);
            this.boardPanel.TabIndex = 0;
            this.boardPanel.DoubleClick += new System.EventHandler(this.boardPanel_DoubleClick);
            this.boardPanel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.boardPanel_MouseClick);
            // 
            // button26
            // 
            this.button26.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button26.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button26.ForeColor = System.Drawing.Color.White;
            this.button26.Location = new System.Drawing.Point(1, 32);
            this.button26.Margin = new System.Windows.Forms.Padding(0);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(47, 30);
            this.button26.TabIndex = 25;
            this.button26.Text = "1";
            this.button26.UseVisualStyleBackColor = false;
            // 
            // button25
            // 
            this.button25.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button25.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button25.ForeColor = System.Drawing.Color.White;
            this.button25.Location = new System.Drawing.Point(836, 1);
            this.button25.Margin = new System.Windows.Forms.Padding(0);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(46, 30);
            this.button25.TabIndex = 24;
            this.button25.Text = "X";
            this.button25.UseVisualStyleBackColor = false;
            // 
            // button24
            // 
            this.button24.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button24.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button24.ForeColor = System.Drawing.Color.White;
            this.button24.Location = new System.Drawing.Point(797, 1);
            this.button24.Margin = new System.Windows.Forms.Padding(0);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(38, 30);
            this.button24.TabIndex = 23;
            this.button24.Text = "W";
            this.button24.UseVisualStyleBackColor = false;
            // 
            // button23
            // 
            this.button23.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button23.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button23.ForeColor = System.Drawing.Color.White;
            this.button23.Location = new System.Drawing.Point(763, 1);
            this.button23.Margin = new System.Windows.Forms.Padding(0);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(33, 30);
            this.button23.TabIndex = 22;
            this.button23.Text = "V";
            this.button23.UseVisualStyleBackColor = false;
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button22.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.ForeColor = System.Drawing.Color.White;
            this.button22.Location = new System.Drawing.Point(729, 1);
            this.button22.Margin = new System.Windows.Forms.Padding(0);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(33, 30);
            this.button22.TabIndex = 21;
            this.button22.Text = "U";
            this.button22.UseVisualStyleBackColor = false;
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button21.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button21.ForeColor = System.Drawing.Color.White;
            this.button21.Location = new System.Drawing.Point(695, 1);
            this.button21.Margin = new System.Windows.Forms.Padding(0);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(33, 30);
            this.button21.TabIndex = 20;
            this.button21.Text = "T";
            this.button21.UseVisualStyleBackColor = false;
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button20.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.ForeColor = System.Drawing.Color.White;
            this.button20.Location = new System.Drawing.Point(661, 1);
            this.button20.Margin = new System.Windows.Forms.Padding(0);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(33, 30);
            this.button20.TabIndex = 19;
            this.button20.Text = "S";
            this.button20.UseVisualStyleBackColor = false;
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button19.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button19.ForeColor = System.Drawing.Color.White;
            this.button19.Location = new System.Drawing.Point(627, 1);
            this.button19.Margin = new System.Windows.Forms.Padding(0);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(33, 30);
            this.button19.TabIndex = 18;
            this.button19.Text = "R";
            this.button19.UseVisualStyleBackColor = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button18.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ForeColor = System.Drawing.Color.White;
            this.button18.Location = new System.Drawing.Point(593, 1);
            this.button18.Margin = new System.Windows.Forms.Padding(0);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(33, 30);
            this.button18.TabIndex = 17;
            this.button18.Text = "Q";
            this.button18.UseVisualStyleBackColor = false;
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button17.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.ForeColor = System.Drawing.Color.White;
            this.button17.Location = new System.Drawing.Point(559, 1);
            this.button17.Margin = new System.Windows.Forms.Padding(0);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(33, 30);
            this.button17.TabIndex = 16;
            this.button17.Text = "P";
            this.button17.UseVisualStyleBackColor = false;
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button16.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.ForeColor = System.Drawing.Color.White;
            this.button16.Location = new System.Drawing.Point(525, 1);
            this.button16.Margin = new System.Windows.Forms.Padding(0);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(33, 30);
            this.button16.TabIndex = 15;
            this.button16.Text = "O";
            this.button16.UseVisualStyleBackColor = false;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button15.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.ForeColor = System.Drawing.Color.White;
            this.button15.Location = new System.Drawing.Point(491, 1);
            this.button15.Margin = new System.Windows.Forms.Padding(0);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(33, 30);
            this.button15.TabIndex = 14;
            this.button15.Text = "N";
            this.button15.UseVisualStyleBackColor = false;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button14.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.ForeColor = System.Drawing.Color.White;
            this.button14.Location = new System.Drawing.Point(457, 1);
            this.button14.Margin = new System.Windows.Forms.Padding(0);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(33, 30);
            this.button14.TabIndex = 13;
            this.button14.Text = "M";
            this.button14.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button13.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(423, 1);
            this.button13.Margin = new System.Windows.Forms.Padding(0);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(33, 30);
            this.button13.TabIndex = 12;
            this.button13.Text = "L";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button12.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.White;
            this.button12.Location = new System.Drawing.Point(389, 1);
            this.button12.Margin = new System.Windows.Forms.Padding(0);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(33, 30);
            this.button12.TabIndex = 11;
            this.button12.Text = "K";
            this.button12.UseVisualStyleBackColor = false;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button11.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.Color.White;
            this.button11.Location = new System.Drawing.Point(355, 1);
            this.button11.Margin = new System.Windows.Forms.Padding(0);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(33, 30);
            this.button11.TabIndex = 10;
            this.button11.Text = "J";
            this.button11.UseVisualStyleBackColor = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button10.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ForeColor = System.Drawing.Color.White;
            this.button10.Location = new System.Drawing.Point(321, 1);
            this.button10.Margin = new System.Windows.Forms.Padding(0);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(33, 30);
            this.button10.TabIndex = 9;
            this.button10.Text = "I";
            this.button10.UseVisualStyleBackColor = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button9.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(287, 1);
            this.button9.Margin = new System.Windows.Forms.Padding(0);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(33, 30);
            this.button9.TabIndex = 8;
            this.button9.Text = "H";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(253, 1);
            this.button8.Margin = new System.Windows.Forms.Padding(0);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(33, 30);
            this.button8.TabIndex = 7;
            this.button8.Text = "G";
            this.button8.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button7.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(219, 1);
            this.button7.Margin = new System.Windows.Forms.Padding(0);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(33, 30);
            this.button7.TabIndex = 6;
            this.button7.Text = "F";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button6.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(185, 1);
            this.button6.Margin = new System.Windows.Forms.Padding(0);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(33, 30);
            this.button6.TabIndex = 5;
            this.button6.Text = "E";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button5.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(151, 1);
            this.button5.Margin = new System.Windows.Forms.Padding(0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(33, 30);
            this.button5.TabIndex = 4;
            this.button5.Text = "D";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(117, 1);
            this.button4.Margin = new System.Windows.Forms.Padding(0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(33, 30);
            this.button4.TabIndex = 3;
            this.button4.Text = "C";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(83, 1);
            this.button3.Margin = new System.Windows.Forms.Padding(0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(33, 30);
            this.button3.TabIndex = 2;
            this.button3.Text = "B";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(49, 1);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 30);
            this.button1.TabIndex = 0;
            this.button1.Text = "A";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(1, 63);
            this.button2.Margin = new System.Windows.Forms.Padding(0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(47, 30);
            this.button2.TabIndex = 26;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button27
            // 
            this.button27.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button27.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button27.ForeColor = System.Drawing.Color.White;
            this.button27.Location = new System.Drawing.Point(1, 94);
            this.button27.Margin = new System.Windows.Forms.Padding(0);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(47, 30);
            this.button27.TabIndex = 27;
            this.button27.Text = "3";
            this.button27.UseVisualStyleBackColor = false;
            // 
            // button28
            // 
            this.button28.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button28.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button28.ForeColor = System.Drawing.Color.White;
            this.button28.Location = new System.Drawing.Point(1, 125);
            this.button28.Margin = new System.Windows.Forms.Padding(0);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(47, 30);
            this.button28.TabIndex = 28;
            this.button28.Text = "4";
            this.button28.UseVisualStyleBackColor = false;
            // 
            // button29
            // 
            this.button29.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button29.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button29.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button29.ForeColor = System.Drawing.Color.White;
            this.button29.Location = new System.Drawing.Point(1, 156);
            this.button29.Margin = new System.Windows.Forms.Padding(0);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(47, 30);
            this.button29.TabIndex = 29;
            this.button29.Text = "5";
            this.button29.UseVisualStyleBackColor = false;
            // 
            // button30
            // 
            this.button30.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button30.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button30.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button30.ForeColor = System.Drawing.Color.White;
            this.button30.Location = new System.Drawing.Point(1, 187);
            this.button30.Margin = new System.Windows.Forms.Padding(0);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(47, 30);
            this.button30.TabIndex = 30;
            this.button30.Text = "6";
            this.button30.UseVisualStyleBackColor = false;
            // 
            // button31
            // 
            this.button31.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button31.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button31.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button31.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button31.ForeColor = System.Drawing.Color.White;
            this.button31.Location = new System.Drawing.Point(1, 218);
            this.button31.Margin = new System.Windows.Forms.Padding(0);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(47, 30);
            this.button31.TabIndex = 31;
            this.button31.Text = "7";
            this.button31.UseVisualStyleBackColor = false;
            // 
            // button32
            // 
            this.button32.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button32.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button32.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button32.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button32.ForeColor = System.Drawing.Color.White;
            this.button32.Location = new System.Drawing.Point(1, 249);
            this.button32.Margin = new System.Windows.Forms.Padding(0);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(47, 30);
            this.button32.TabIndex = 32;
            this.button32.Text = "8";
            this.button32.UseVisualStyleBackColor = false;
            // 
            // button33
            // 
            this.button33.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button33.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button33.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button33.ForeColor = System.Drawing.Color.White;
            this.button33.Location = new System.Drawing.Point(1, 280);
            this.button33.Margin = new System.Windows.Forms.Padding(0);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(47, 30);
            this.button33.TabIndex = 33;
            this.button33.Text = "9";
            this.button33.UseVisualStyleBackColor = false;
            // 
            // button34
            // 
            this.button34.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button34.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button34.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button34.ForeColor = System.Drawing.Color.White;
            this.button34.Location = new System.Drawing.Point(1, 311);
            this.button34.Margin = new System.Windows.Forms.Padding(0);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(47, 30);
            this.button34.TabIndex = 34;
            this.button34.Text = "10";
            this.button34.UseVisualStyleBackColor = false;
            // 
            // button35
            // 
            this.button35.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button35.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button35.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button35.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button35.ForeColor = System.Drawing.Color.White;
            this.button35.Location = new System.Drawing.Point(1, 342);
            this.button35.Margin = new System.Windows.Forms.Padding(0);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(47, 30);
            this.button35.TabIndex = 35;
            this.button35.Text = "11";
            this.button35.UseVisualStyleBackColor = false;
            // 
            // button36
            // 
            this.button36.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button36.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button36.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button36.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button36.ForeColor = System.Drawing.Color.White;
            this.button36.Location = new System.Drawing.Point(1, 373);
            this.button36.Margin = new System.Windows.Forms.Padding(0);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(47, 30);
            this.button36.TabIndex = 36;
            this.button36.Text = "12";
            this.button36.UseVisualStyleBackColor = false;
            // 
            // button37
            // 
            this.button37.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button37.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button37.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button37.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button37.ForeColor = System.Drawing.Color.White;
            this.button37.Location = new System.Drawing.Point(1, 404);
            this.button37.Margin = new System.Windows.Forms.Padding(0);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(47, 30);
            this.button37.TabIndex = 37;
            this.button37.Text = "13";
            this.button37.UseVisualStyleBackColor = false;
            // 
            // button38
            // 
            this.button38.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button38.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button38.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button38.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button38.ForeColor = System.Drawing.Color.White;
            this.button38.Location = new System.Drawing.Point(1, 435);
            this.button38.Margin = new System.Windows.Forms.Padding(0);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(47, 30);
            this.button38.TabIndex = 38;
            this.button38.Text = "14";
            this.button38.UseVisualStyleBackColor = false;
            // 
            // button39
            // 
            this.button39.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button39.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button39.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button39.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button39.ForeColor = System.Drawing.Color.White;
            this.button39.Location = new System.Drawing.Point(1, 466);
            this.button39.Margin = new System.Windows.Forms.Padding(0);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(47, 30);
            this.button39.TabIndex = 39;
            this.button39.Text = "15";
            this.button39.UseVisualStyleBackColor = false;
            // 
            // button40
            // 
            this.button40.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button40.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button40.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button40.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button40.ForeColor = System.Drawing.Color.White;
            this.button40.Location = new System.Drawing.Point(1, 497);
            this.button40.Margin = new System.Windows.Forms.Padding(0);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(47, 30);
            this.button40.TabIndex = 40;
            this.button40.Text = "16";
            this.button40.UseVisualStyleBackColor = false;
            // 
            // button41
            // 
            this.button41.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button41.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button41.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button41.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button41.ForeColor = System.Drawing.Color.White;
            this.button41.Location = new System.Drawing.Point(1, 528);
            this.button41.Margin = new System.Windows.Forms.Padding(0);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(47, 30);
            this.button41.TabIndex = 41;
            this.button41.Text = "17";
            this.button41.UseVisualStyleBackColor = false;
            // 
            // button42
            // 
            this.button42.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button42.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button42.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button42.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button42.ForeColor = System.Drawing.Color.White;
            this.button42.Location = new System.Drawing.Point(1, 559);
            this.button42.Margin = new System.Windows.Forms.Padding(0);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(47, 30);
            this.button42.TabIndex = 42;
            this.button42.Text = "18";
            this.button42.UseVisualStyleBackColor = false;
            // 
            // button43
            // 
            this.button43.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button43.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button43.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button43.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button43.ForeColor = System.Drawing.Color.White;
            this.button43.Location = new System.Drawing.Point(1, 590);
            this.button43.Margin = new System.Windows.Forms.Padding(0);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(47, 30);
            this.button43.TabIndex = 43;
            this.button43.Text = "19";
            this.button43.UseVisualStyleBackColor = false;
            // 
            // button44
            // 
            this.button44.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button44.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button44.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button44.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button44.ForeColor = System.Drawing.Color.White;
            this.button44.Location = new System.Drawing.Point(1, 621);
            this.button44.Margin = new System.Windows.Forms.Padding(0);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(47, 30);
            this.button44.TabIndex = 44;
            this.button44.Text = "20";
            this.button44.UseVisualStyleBackColor = false;
            // 
            // button45
            // 
            this.button45.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button45.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button45.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button45.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button45.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button45.ForeColor = System.Drawing.Color.White;
            this.button45.Location = new System.Drawing.Point(1, 652);
            this.button45.Margin = new System.Windows.Forms.Padding(0);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(47, 30);
            this.button45.TabIndex = 45;
            this.button45.Text = "21";
            this.button45.UseVisualStyleBackColor = false;
            // 
            // button46
            // 
            this.button46.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button46.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button46.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button46.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button46.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button46.ForeColor = System.Drawing.Color.White;
            this.button46.Location = new System.Drawing.Point(1, 683);
            this.button46.Margin = new System.Windows.Forms.Padding(0);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(47, 30);
            this.button46.TabIndex = 46;
            this.button46.Text = "22";
            this.button46.UseVisualStyleBackColor = false;
            // 
            // button47
            // 
            this.button47.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button47.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button47.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button47.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button47.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button47.ForeColor = System.Drawing.Color.White;
            this.button47.Location = new System.Drawing.Point(1, 714);
            this.button47.Margin = new System.Windows.Forms.Padding(0);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(47, 30);
            this.button47.TabIndex = 47;
            this.button47.Text = "23";
            this.button47.UseVisualStyleBackColor = false;
            // 
            // button48
            // 
            this.button48.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button48.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button48.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button48.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button48.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button48.ForeColor = System.Drawing.Color.White;
            this.button48.Location = new System.Drawing.Point(1, 745);
            this.button48.Margin = new System.Windows.Forms.Padding(0);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(47, 38);
            this.button48.TabIndex = 48;
            this.button48.Text = "24";
            this.button48.UseVisualStyleBackColor = false;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.clearButton);
            this.splitContainer3.Panel1.Controls.Add(this.selectedLPCost);
            this.splitContainer3.Panel1.Controls.Add(this.selectedContent4);
            this.splitContainer3.Panel1.Controls.Add(this.selectedContent3);
            this.splitContainer3.Panel1.Controls.Add(this.selectedContent2);
            this.splitContainer3.Panel1.Controls.Add(this.selectedContent1);
            this.splitContainer3.Panel1.Controls.Add(this.selectedCellText);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.AutoScroll = true;
            this.splitContainer3.Panel2.Controls.Add(this.logLabel);
            this.splitContainer3.Panel2.Controls.Add(this.showLogBtn);
            this.splitContainer3.Panel2.Controls.Add(this.insertButton);
            this.splitContainer3.Panel2.Controls.Add(this.listLicence4);
            this.splitContainer3.Panel2.Controls.Add(this.listLicence3);
            this.splitContainer3.Panel2.Controls.Add(this.listLicence2);
            this.splitContainer3.Panel2.Controls.Add(this.listLicence1);
            this.splitContainer3.Panel2.Controls.Add(this.licenceList);
            this.splitContainer3.Panel2.Controls.Add(this.label1);
            this.splitContainer3.Panel2.Controls.Add(this.searchBox);
            this.splitContainer3.Size = new System.Drawing.Size(365, 796);
            this.splitContainer3.SplitterDistance = 217;
            this.splitContainer3.TabIndex = 0;
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(193, 179);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(168, 29);
            this.clearButton.TabIndex = 6;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // selectedLPCost
            // 
            this.selectedLPCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectedLPCost.Location = new System.Drawing.Point(3, 179);
            this.selectedLPCost.Name = "selectedLPCost";
            this.selectedLPCost.ReadOnly = true;
            this.selectedLPCost.Size = new System.Drawing.Size(184, 29);
            this.selectedLPCost.TabIndex = 5;
            // 
            // selectedContent4
            // 
            this.selectedContent4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectedContent4.Location = new System.Drawing.Point(3, 144);
            this.selectedContent4.Name = "selectedContent4";
            this.selectedContent4.ReadOnly = true;
            this.selectedContent4.Size = new System.Drawing.Size(358, 29);
            this.selectedContent4.TabIndex = 4;
            // 
            // selectedContent3
            // 
            this.selectedContent3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectedContent3.Location = new System.Drawing.Point(3, 109);
            this.selectedContent3.Name = "selectedContent3";
            this.selectedContent3.ReadOnly = true;
            this.selectedContent3.Size = new System.Drawing.Size(358, 29);
            this.selectedContent3.TabIndex = 3;
            // 
            // selectedContent2
            // 
            this.selectedContent2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectedContent2.Location = new System.Drawing.Point(3, 74);
            this.selectedContent2.Name = "selectedContent2";
            this.selectedContent2.ReadOnly = true;
            this.selectedContent2.Size = new System.Drawing.Size(358, 29);
            this.selectedContent2.TabIndex = 2;
            // 
            // selectedContent1
            // 
            this.selectedContent1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectedContent1.Location = new System.Drawing.Point(4, 39);
            this.selectedContent1.Name = "selectedContent1";
            this.selectedContent1.ReadOnly = true;
            this.selectedContent1.Size = new System.Drawing.Size(358, 29);
            this.selectedContent1.TabIndex = 1;
            // 
            // selectedCellText
            // 
            this.selectedCellText.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectedCellText.Location = new System.Drawing.Point(4, 4);
            this.selectedCellText.Name = "selectedCellText";
            this.selectedCellText.ReadOnly = true;
            this.selectedCellText.Size = new System.Drawing.Size(358, 29);
            this.selectedCellText.TabIndex = 0;
            this.selectedCellText.Text = "[A1]";
            // 
            // logLabel
            // 
            this.logLabel.AutoSize = true;
            this.logLabel.Location = new System.Drawing.Point(4, 555);
            this.logLabel.Name = "logLabel";
            this.logLabel.Size = new System.Drawing.Size(16, 13);
            this.logLabel.TabIndex = 16;
            this.logLabel.Text = "...";
            // 
            // showLogBtn
            // 
            this.showLogBtn.Location = new System.Drawing.Point(286, 548);
            this.showLogBtn.Name = "showLogBtn";
            this.showLogBtn.Size = new System.Drawing.Size(75, 26);
            this.showLogBtn.TabIndex = 15;
            this.showLogBtn.Text = "Show Log";
            this.showLogBtn.UseVisualStyleBackColor = true;
            this.showLogBtn.Click += new System.EventHandler(this.showLogBtn_Click);
            // 
            // insertButton
            // 
            this.insertButton.Location = new System.Drawing.Point(5, 513);
            this.insertButton.Name = "insertButton";
            this.insertButton.Size = new System.Drawing.Size(357, 29);
            this.insertButton.TabIndex = 14;
            this.insertButton.Text = "Insert";
            this.insertButton.UseVisualStyleBackColor = true;
            this.insertButton.Click += new System.EventHandler(this.insertButton_Click);
            // 
            // listLicence4
            // 
            this.listLicence4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listLicence4.Location = new System.Drawing.Point(6, 479);
            this.listLicence4.Name = "listLicence4";
            this.listLicence4.ReadOnly = true;
            this.listLicence4.Size = new System.Drawing.Size(358, 29);
            this.listLicence4.TabIndex = 13;
            // 
            // listLicence3
            // 
            this.listLicence3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listLicence3.Location = new System.Drawing.Point(6, 444);
            this.listLicence3.Name = "listLicence3";
            this.listLicence3.ReadOnly = true;
            this.listLicence3.Size = new System.Drawing.Size(358, 29);
            this.listLicence3.TabIndex = 12;
            // 
            // listLicence2
            // 
            this.listLicence2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listLicence2.Location = new System.Drawing.Point(6, 409);
            this.listLicence2.Name = "listLicence2";
            this.listLicence2.ReadOnly = true;
            this.listLicence2.Size = new System.Drawing.Size(358, 29);
            this.listLicence2.TabIndex = 11;
            // 
            // listLicence1
            // 
            this.listLicence1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listLicence1.Location = new System.Drawing.Point(7, 374);
            this.listLicence1.Name = "listLicence1";
            this.listLicence1.ReadOnly = true;
            this.listLicence1.Size = new System.Drawing.Size(358, 29);
            this.listLicence1.TabIndex = 10;
            // 
            // licenceList
            // 
            this.licenceList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.codeHeader,
            this.licenceHeader,
            this.lpHeader,
            this.typeHeader,
            this.insertedPos});
            this.licenceList.FullRowSelect = true;
            this.licenceList.GridLines = true;
            this.licenceList.Location = new System.Drawing.Point(4, 51);
            this.licenceList.MultiSelect = false;
            this.licenceList.Name = "licenceList";
            this.licenceList.Size = new System.Drawing.Size(357, 317);
            this.licenceList.TabIndex = 9;
            this.licenceList.UseCompatibleStateImageBehavior = false;
            this.licenceList.View = System.Windows.Forms.View.Details;
            this.licenceList.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.licenceList_ColumnClick);
            this.licenceList.SelectedIndexChanged += new System.EventHandler(this.licenceList_SelectedIndexChanged);
            // 
            // codeHeader
            // 
            this.codeHeader.Text = "Code";
            this.codeHeader.Width = 49;
            // 
            // licenceHeader
            // 
            this.licenceHeader.Text = "Licence";
            this.licenceHeader.Width = 155;
            // 
            // lpHeader
            // 
            this.lpHeader.Text = "LP";
            this.lpHeader.Width = 41;
            // 
            // typeHeader
            // 
            this.typeHeader.Text = "Type";
            this.typeHeader.Width = 68;
            // 
            // insertedPos
            // 
            this.insertedPos.Text = "@";
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(4, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(357, 2);
            this.label1.TabIndex = 8;
            // 
            // searchBox
            // 
            this.searchBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchBox.Location = new System.Drawing.Point(4, 15);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(358, 29);
            this.searchBox.TabIndex = 7;
            this.searchBox.TextChanged += new System.EventHandler(this.searchBox_TextChanged);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "bin";
            this.saveFileDialog1.Filter = "Binary Files | *.bin";
            this.saveFileDialog1.Title = "Save Board";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "bin";
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Binary | *.bin";
            // 
            // button51
            // 
            this.button51.Location = new System.Drawing.Point(339, 8);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(144, 23);
            this.button51.TabIndex = 12;
            this.button51.Text = "Rotate Left";
            this.button51.UseVisualStyleBackColor = true;
            this.button51.Click += new System.EventHandler(this.button51_Click);
            // 
            // button52
            // 
            this.button52.Location = new System.Drawing.Point(339, 35);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(144, 23);
            this.button52.TabIndex = 14;
            this.button52.Text = "Flip Horizontally";
            this.button52.UseVisualStyleBackColor = true;
            this.button52.Click += new System.EventHandler(this.button52_Click);
            // 
            // button53
            // 
            this.button53.Location = new System.Drawing.Point(489, 35);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(144, 23);
            this.button53.TabIndex = 13;
            this.button53.Text = "Flip Vertically";
            this.button53.UseVisualStyleBackColor = true;
            this.button53.Click += new System.EventHandler(this.button53_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 861);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Zodiac Planner";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.quitBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aboutBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newButton)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.boardPanel.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.TextBox selectedLPCost;
        private System.Windows.Forms.TextBox selectedContent4;
        private System.Windows.Forms.TextBox selectedContent3;
        private System.Windows.Forms.TextBox selectedContent2;
        private System.Windows.Forms.TextBox selectedContent1;
        private System.Windows.Forms.TextBox selectedCellText;
        private System.Windows.Forms.Button insertButton;
        private System.Windows.Forms.TextBox listLicence4;
        private System.Windows.Forms.TextBox listLicence3;
        private System.Windows.Forms.TextBox listLicence2;
        private System.Windows.Forms.TextBox listLicence1;
        private System.Windows.Forms.ListView licenceList;
        private System.Windows.Forms.ColumnHeader codeHeader;
        private System.Windows.Forms.ColumnHeader licenceHeader;
        private System.Windows.Forms.ColumnHeader lpHeader;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox searchBox;
        private System.Windows.Forms.CheckBox rightClick;
        private System.Windows.Forms.CheckBox doubleClick;
        private System.Windows.Forms.PictureBox newButton;
        private System.Windows.Forms.PictureBox loadBtn;
        private System.Windows.Forms.PictureBox saveBtn;
        private System.Windows.Forms.PictureBox aboutBtn;
        private System.Windows.Forms.PictureBox quitBtn;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TableLayoutPanel boardPanel;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Button toggleColor;
        private System.Windows.Forms.ColumnHeader typeHeader;
        private System.Windows.Forms.Button colorSettings;
        private System.Windows.Forms.Label logLabel;
        private System.Windows.Forms.Button showLogBtn;
        private System.Windows.Forms.CheckBox outputLogCheck;
        private System.Windows.Forms.ColumnHeader insertedPos;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Button button53;
    }
}

