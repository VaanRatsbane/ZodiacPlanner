﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZodiacPlanner
{
    public class Licence
    {
        public string pair1 { get; private set; }
        public string pair2 { get; private set; }
        public int lpCost { get; private set; }
        public LicenceType licenceType { get; private set; }
        public string name { get; private set; }
        public string[] contents { get; private set; }

        public bool inserted { get; private set; }
        public string position { get { return listViewItemContent[4]; } }
        Color color;

        string[] listViewItemContent;

        public Licence(LicenceType licenceType, string pair1, string pair2, int lpCost, string name, string[] contents)
        {
            this.pair1 = pair1;
            this.pair2 = pair2;
            this.name = name;
            this.contents = contents;
            this.lpCost = lpCost;
            this.licenceType = licenceType;
            listViewItemContent = new string[] { pair1 + pair2, name, lpCost.ToString(), ParseType(licenceType), String.Empty };
            inserted = false;
            color = Program.colors.Get(licenceType);
        }

        /// <summary>
        /// Used when loading from vbf.
        /// </summary>
        public Licence(byte[] d, LoadedStrings strings)
        {
            var licenceId = (ushort)(BitConverter.ToUInt16(d, 0) - 0x1800);
            pair1 = d[0].ToString("X2");
            pair2 = ((byte)(d[1] - 0x18)).ToString("X2");

            var licenceGroup = d[2];
            //var unknown = d[3];
            lpCost = d[4];
            var licenceSubgroup = d[5];

            name = strings.GetLicenceName(licenceId);

            if (licenceId < 18) licenceType = LicenceType.QUICKENING;
            else if (licenceId < 31) licenceType = LicenceType.SUMMON;
            else if (licenceId < 32) licenceType = LicenceType.ESSENTIALS;
            else if (licenceId < 40) licenceType = LicenceType.SWORDS;
            else if (licenceId < 45) licenceType = LicenceType.GSWORDS;
            else if (licenceId < 50) licenceType = LicenceType.KATANAS;
            else if (licenceId < 53) licenceType = LicenceType.NINJA;
            else if (licenceId < 60) licenceType = LicenceType.SPEARS;
            else if (licenceId < 66) licenceType = LicenceType.POLES;
            else if (licenceId < 73) licenceType = LicenceType.BOWS;
            else if (licenceId < 77) licenceType = LicenceType.CROSSBOWS;
            else if (licenceId < 83) licenceType = LicenceType.GUNS;
            else if (licenceId < 90) licenceType = LicenceType.AXES;
            else if (licenceId < 96) licenceType = LicenceType.DAGGERS;
            else if (licenceId < 101) licenceType = LicenceType.RODS;
            else if (licenceId < 106) licenceType = LicenceType.STAVES;
            else if (licenceId < 111) licenceType = LicenceType.MACES;
            else if (licenceId < 114) licenceType = LicenceType.MEASURES;
            else if (licenceId < 117) licenceType = LicenceType.BOMBS;
            else if (licenceId < 126) licenceType = LicenceType.SHIELDS;
            else if (licenceId < 137) licenceType = LicenceType.HEAVY;
            else if (licenceId < 149) licenceType = LicenceType.LIGHT;
            else if (licenceId < 161) licenceType = LicenceType.MYSTIC;
            else if (licenceId < 182) licenceType = LicenceType.ACCESSORIES;
            else if (licenceId < 190) licenceType = LicenceType.WHITE;
            else if (licenceId < 198) licenceType = LicenceType.BLACK;
            else if (licenceId < 205) licenceType = LicenceType.TIME;
            else if (licenceId < 208) licenceType = LicenceType.GREEN;
            else if (licenceId < 212) licenceType = LicenceType.WHITE;
            else if (licenceId < 215) licenceType = LicenceType.ARCANE;
            else if (licenceId < 218) licenceType = LicenceType.BLACK;
            else if (licenceId < 219) licenceType = LicenceType.TIME;
            else if (licenceId < 266) licenceType = LicenceType.AUG;
            else if (licenceId < 276) licenceType = LicenceType.GAMBIT;
            else if (licenceId < 300) licenceType = LicenceType.TECH;
            else if (licenceId < 329) licenceType = LicenceType.AUG;
            else if (licenceId < 332) licenceType = LicenceType.SWORDS;
            else if (licenceId < 334) licenceType = LicenceType.GSWORDS;
            else if (licenceId < 336) licenceType = LicenceType.KATANAS;
            else if (licenceId < 338) licenceType = LicenceType.NINJA;
            else if (licenceId < 340) licenceType = LicenceType.POLES;
            else if (licenceId < 342) licenceType = LicenceType.BOWS;
            else if (licenceId < 343) licenceType = LicenceType.GUNS;
            else if (licenceId < 344) licenceType = LicenceType.AXES;
            else if (licenceId < 345) licenceType = LicenceType.DAGGERS;
            else if (licenceId < 346) licenceType = LicenceType.STAVES;
            else if (licenceId < 347) licenceType = LicenceType.MEASURES;
            else if (licenceId < 348) licenceType = LicenceType.BOMBS;
            else if (licenceId < 349) licenceType = LicenceType.SHIELDS;
            else if (licenceId < 351) licenceType = LicenceType.HEAVY;
            else if (licenceId < 352) licenceType = LicenceType.LIGHT;
            else if (licenceId < 353) licenceType = LicenceType.MYSTIC;
            else if (licenceId < 354) licenceType = LicenceType.ACCESSORIES;
            else if (licenceId < 356) licenceType = LicenceType.WHITE;
            else if (licenceId < 357) licenceType = LicenceType.BLACK;
            else if (licenceId < 358) licenceType = LicenceType.TIME;
            else if (licenceId < 359) licenceType = LicenceType.BLACK;
            else if (licenceId < 360) licenceType = LicenceType.TIME;
            else if (licenceId < 361) licenceType = LicenceType.SECONDBOARD;

            //var licenceMechanic = d[6];
            //var chars = d[7];

            var contentList = new List<string>();
            for (int i = 8; i < 24; i+=2)
            {
                var id = BitConverter.ToUInt16(d, i);
                if (id == 0xFFFF) break;
                try
                {
                    contentList.Add(strings.GetComponentString(licenceGroup, licenceSubgroup, id));
                }
                catch(KeyNotFoundException)
                {
                    //if it doesn't find an id, then it is a dead reference (which doesn't break the licence system)
                }
            }
            contents = contentList.ToArray();

            listViewItemContent = new string[] { pair1 + pair2, name, lpCost.ToString(), ParseType(licenceType), String.Empty };
            inserted = false;
            color = Program.colors.Get(licenceType);
        }

        /// <summary>
        /// For quickenings.
        /// </summary>
        public Licence(LoadedStrings strings, byte quickCounter, byte lpCost)
        {
            this.pair1 = quickCounter.ToString("X2");
            this.pair2 = "00";
            this.lpCost = lpCost;
            name = $"{strings.GetLicenceName(361)}-{(char)(65+quickCounter)}";
            licenceType = LicenceType.QUICKENING;
            contents = new string[0];
            listViewItemContent = new string[] { pair1 + pair2, name, lpCost.ToString(), ParseType(licenceType), String.Empty };
            inserted = false;
            color = Program.colors.Get(licenceType);
        }

        public void Insert(string pos)
        {
            inserted = true;
            listViewItemContent[4] = pos;
        }

        public void Clear()
        {
            inserted = false;
            listViewItemContent[4] = String.Empty;
        }

        public bool Query(string query)
        {
            query = query.ToLower();

            if (name.ToLower().Contains(query))
                return true;
            if (contents.Length > 0 && contents[0].ToLower().Contains(query))
                return true;
            if (contents.Length > 1 && contents[1].ToLower().Contains(query))
                return true;
            if (contents.Length > 2 && contents[2].ToLower().Contains(query))
                return true;
            if (contents.Length > 3 && contents[3].ToLower().Contains(query))
                return true;
            return false;
        }

        public ListViewItem GetListViewItem()
        {
            return new ListViewItem(listViewItemContent)
            {
                BackColor = inserted ? Color.Gold : Color.White,
                Tag = this
            };
        }

        public void ChangeCell(Button btn)
        {
            btn.BackColor = color;
            btn.Tag = this;
        }

        public void UpdateColor()
        {
            color = Program.colors.Get(licenceType);
        }

        static Dictionary<LicenceType, string> dict = new Dictionary<LicenceType, string>(33)
        {
            {LicenceType.QUICKENING, ""}, {LicenceType.SUMMON, ""}, {LicenceType.ESSENTIALS, ""},
            {LicenceType.SWORDS, ""}, {LicenceType.GSWORDS, ""}, {LicenceType.KATANAS, ""},
            {LicenceType.NINJA, ""}, {LicenceType.SPEARS, ""}, {LicenceType.POLES, ""},
            {LicenceType.BOWS, ""}, {LicenceType.CROSSBOWS, ""}, {LicenceType.GUNS, ""},
            {LicenceType.AXES, ""}, {LicenceType.DAGGERS, ""}, {LicenceType.RODS, ""},
            {LicenceType.STAVES, ""}, {LicenceType.MACES, ""}, {LicenceType.MEASURES, ""},
            {LicenceType.BOMBS, ""}, {LicenceType.SHIELDS, ""}, {LicenceType.HEAVY, ""},
            {LicenceType.LIGHT, ""}, {LicenceType.MYSTIC, ""}, {LicenceType.ACCESSORIES, ""},
            {LicenceType.WHITE, ""}, {LicenceType.BLACK, ""}, {LicenceType.TIME, ""},
            {LicenceType.GREEN, ""}, {LicenceType.ARCANE, ""}, {LicenceType.AUG, ""},
            {LicenceType.GAMBIT, ""}, {LicenceType.TECH, ""}, {LicenceType.SECONDBOARD, ""}
        };

        /// <summary>
        /// Defines the licence type names.
        /// </summary>
        /// <param name="names">33 size licence name array.</param>
        public static void SetDictionary(string[] names)
        {
            for (int i = 0; i < 33; i++)
                dict[dict.Keys.ElementAt(i)] = names[i];
        }

        static string ParseType(LicenceType t)
        {
            return dict[t];
        }

    }

    public enum LicenceType
    {
        QUICKENING, SUMMON, ESSENTIALS,
        SWORDS, GSWORDS, KATANAS,
        NINJA, SPEARS, POLES,
        BOWS, CROSSBOWS, GUNS,
        AXES, DAGGERS, RODS,
        STAVES, MACES, MEASURES,
        BOMBS, SHIELDS, HEAVY,
        LIGHT, MYSTIC, ACCESSORIES,
        WHITE, BLACK, TIME,
        GREEN, ARCANE, AUG,
        GAMBIT, TECH, SECONDBOARD
    }
}
