﻿namespace ZodiacPlanner
{
    partial class ColorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.quickenings = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.summons = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.essentials = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.swords = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.greatswords = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.katanas = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.ninjaswords = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.spears = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.poles = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.bows = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.crossbows = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.light = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.heavy = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.shields = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.handbombs = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.measures = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.maces = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.staves = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.rods = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.daggers = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.axes = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.guns = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.secondboard = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.technicks = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.gambits = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.augments = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.arcane = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.green = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.time = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.blm = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.whm = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.accessories = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.mystic = new System.Windows.Forms.Button();
            this.closeBtn = new System.Windows.Forms.Button();
            this.revertDefaults = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // quickenings
            // 
            this.quickenings.Location = new System.Drawing.Point(13, 13);
            this.quickenings.Name = "quickenings";
            this.quickenings.Size = new System.Drawing.Size(25, 25);
            this.quickenings.TabIndex = 0;
            this.quickenings.UseVisualStyleBackColor = false;
            this.quickenings.Click += new System.EventHandler(this.quickenings_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Quickenings";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Summons";
            // 
            // summons
            // 
            this.summons.Location = new System.Drawing.Point(13, 44);
            this.summons.Name = "summons";
            this.summons.Size = new System.Drawing.Size(25, 25);
            this.summons.TabIndex = 2;
            this.summons.UseVisualStyleBackColor = false;
            this.summons.Click += new System.EventHandler(this.summons_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Essentials";
            // 
            // essentials
            // 
            this.essentials.Location = new System.Drawing.Point(13, 75);
            this.essentials.Name = "essentials";
            this.essentials.Size = new System.Drawing.Size(25, 25);
            this.essentials.TabIndex = 4;
            this.essentials.UseVisualStyleBackColor = false;
            this.essentials.Click += new System.EventHandler(this.essentials_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Swords";
            // 
            // swords
            // 
            this.swords.Location = new System.Drawing.Point(13, 106);
            this.swords.Name = "swords";
            this.swords.Size = new System.Drawing.Size(25, 25);
            this.swords.TabIndex = 6;
            this.swords.UseVisualStyleBackColor = false;
            this.swords.Click += new System.EventHandler(this.swords_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 143);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Greatswords";
            // 
            // greatswords
            // 
            this.greatswords.Location = new System.Drawing.Point(13, 137);
            this.greatswords.Name = "greatswords";
            this.greatswords.Size = new System.Drawing.Size(25, 25);
            this.greatswords.TabIndex = 8;
            this.greatswords.UseVisualStyleBackColor = false;
            this.greatswords.Click += new System.EventHandler(this.greatswords_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(44, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Katanas";
            // 
            // katanas
            // 
            this.katanas.Location = new System.Drawing.Point(13, 168);
            this.katanas.Name = "katanas";
            this.katanas.Size = new System.Drawing.Size(25, 25);
            this.katanas.TabIndex = 10;
            this.katanas.UseVisualStyleBackColor = false;
            this.katanas.Click += new System.EventHandler(this.katanas_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(44, 205);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Ninja Swords";
            // 
            // ninjaswords
            // 
            this.ninjaswords.Location = new System.Drawing.Point(13, 199);
            this.ninjaswords.Name = "ninjaswords";
            this.ninjaswords.Size = new System.Drawing.Size(25, 25);
            this.ninjaswords.TabIndex = 12;
            this.ninjaswords.UseVisualStyleBackColor = false;
            this.ninjaswords.Click += new System.EventHandler(this.ninjaswords_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(44, 236);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Spears";
            // 
            // spears
            // 
            this.spears.Location = new System.Drawing.Point(13, 230);
            this.spears.Name = "spears";
            this.spears.Size = new System.Drawing.Size(25, 25);
            this.spears.TabIndex = 14;
            this.spears.UseVisualStyleBackColor = false;
            this.spears.Click += new System.EventHandler(this.spears_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(44, 267);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Poles";
            // 
            // poles
            // 
            this.poles.Location = new System.Drawing.Point(13, 261);
            this.poles.Name = "poles";
            this.poles.Size = new System.Drawing.Size(25, 25);
            this.poles.TabIndex = 16;
            this.poles.UseVisualStyleBackColor = false;
            this.poles.Click += new System.EventHandler(this.poles_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(44, 298);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Bows";
            // 
            // bows
            // 
            this.bows.Location = new System.Drawing.Point(13, 292);
            this.bows.Name = "bows";
            this.bows.Size = new System.Drawing.Size(25, 25);
            this.bows.TabIndex = 18;
            this.bows.UseVisualStyleBackColor = false;
            this.bows.Click += new System.EventHandler(this.bows_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(44, 329);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Crossbows";
            // 
            // crossbows
            // 
            this.crossbows.Location = new System.Drawing.Point(13, 323);
            this.crossbows.Name = "crossbows";
            this.crossbows.Size = new System.Drawing.Size(25, 25);
            this.crossbows.TabIndex = 20;
            this.crossbows.UseVisualStyleBackColor = false;
            this.crossbows.Click += new System.EventHandler(this.crossbows_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(196, 329);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 13);
            this.label12.TabIndex = 43;
            this.label12.Text = "Light Armor";
            // 
            // light
            // 
            this.light.Location = new System.Drawing.Point(165, 323);
            this.light.Name = "light";
            this.light.Size = new System.Drawing.Size(25, 25);
            this.light.TabIndex = 42;
            this.light.UseVisualStyleBackColor = false;
            this.light.Click += new System.EventHandler(this.light_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(196, 298);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 13);
            this.label13.TabIndex = 41;
            this.label13.Text = "Heavy Armor";
            // 
            // heavy
            // 
            this.heavy.Location = new System.Drawing.Point(165, 292);
            this.heavy.Name = "heavy";
            this.heavy.Size = new System.Drawing.Size(25, 25);
            this.heavy.TabIndex = 40;
            this.heavy.UseVisualStyleBackColor = false;
            this.heavy.Click += new System.EventHandler(this.heavy_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(196, 267);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 39;
            this.label14.Text = "Shields";
            // 
            // shields
            // 
            this.shields.Location = new System.Drawing.Point(165, 261);
            this.shields.Name = "shields";
            this.shields.Size = new System.Drawing.Size(25, 25);
            this.shields.TabIndex = 38;
            this.shields.UseVisualStyleBackColor = false;
            this.shields.Click += new System.EventHandler(this.shields_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(196, 236);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 13);
            this.label15.TabIndex = 37;
            this.label15.Text = "Hand Bombs";
            // 
            // handbombs
            // 
            this.handbombs.Location = new System.Drawing.Point(165, 230);
            this.handbombs.Name = "handbombs";
            this.handbombs.Size = new System.Drawing.Size(25, 25);
            this.handbombs.TabIndex = 36;
            this.handbombs.UseVisualStyleBackColor = false;
            this.handbombs.Click += new System.EventHandler(this.handbombs_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(196, 205);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 13);
            this.label16.TabIndex = 35;
            this.label16.Text = "Measures";
            // 
            // measures
            // 
            this.measures.Location = new System.Drawing.Point(165, 199);
            this.measures.Name = "measures";
            this.measures.Size = new System.Drawing.Size(25, 25);
            this.measures.TabIndex = 34;
            this.measures.UseVisualStyleBackColor = false;
            this.measures.Click += new System.EventHandler(this.measures_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(196, 174);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(39, 13);
            this.label17.TabIndex = 33;
            this.label17.Text = "Maces";
            // 
            // maces
            // 
            this.maces.Location = new System.Drawing.Point(165, 168);
            this.maces.Name = "maces";
            this.maces.Size = new System.Drawing.Size(25, 25);
            this.maces.TabIndex = 32;
            this.maces.UseVisualStyleBackColor = false;
            this.maces.Click += new System.EventHandler(this.maces_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(196, 143);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 13);
            this.label18.TabIndex = 31;
            this.label18.Text = "Staves";
            // 
            // staves
            // 
            this.staves.Location = new System.Drawing.Point(165, 137);
            this.staves.Name = "staves";
            this.staves.Size = new System.Drawing.Size(25, 25);
            this.staves.TabIndex = 30;
            this.staves.UseVisualStyleBackColor = false;
            this.staves.Click += new System.EventHandler(this.staves_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(196, 112);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 13);
            this.label19.TabIndex = 29;
            this.label19.Text = "Rods";
            // 
            // rods
            // 
            this.rods.Location = new System.Drawing.Point(165, 106);
            this.rods.Name = "rods";
            this.rods.Size = new System.Drawing.Size(25, 25);
            this.rods.TabIndex = 28;
            this.rods.UseVisualStyleBackColor = false;
            this.rods.Click += new System.EventHandler(this.rods_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(196, 81);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 13);
            this.label20.TabIndex = 27;
            this.label20.Text = "Daggers";
            // 
            // daggers
            // 
            this.daggers.Location = new System.Drawing.Point(165, 75);
            this.daggers.Name = "daggers";
            this.daggers.Size = new System.Drawing.Size(25, 25);
            this.daggers.TabIndex = 26;
            this.daggers.UseVisualStyleBackColor = false;
            this.daggers.Click += new System.EventHandler(this.daggers_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(196, 50);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(98, 13);
            this.label21.TabIndex = 25;
            this.label21.Text = "Axes and Hammers";
            // 
            // axes
            // 
            this.axes.Location = new System.Drawing.Point(165, 44);
            this.axes.Name = "axes";
            this.axes.Size = new System.Drawing.Size(25, 25);
            this.axes.TabIndex = 24;
            this.axes.UseVisualStyleBackColor = false;
            this.axes.Click += new System.EventHandler(this.axes_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(196, 19);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(32, 13);
            this.label22.TabIndex = 23;
            this.label22.Text = "Guns";
            // 
            // guns
            // 
            this.guns.Location = new System.Drawing.Point(165, 13);
            this.guns.Name = "guns";
            this.guns.Size = new System.Drawing.Size(25, 25);
            this.guns.TabIndex = 22;
            this.guns.UseVisualStyleBackColor = false;
            this.guns.Click += new System.EventHandler(this.guns_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(351, 329);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(75, 13);
            this.label23.TabIndex = 65;
            this.label23.Text = "Second Board";
            // 
            // secondboard
            // 
            this.secondboard.Location = new System.Drawing.Point(320, 323);
            this.secondboard.Name = "secondboard";
            this.secondboard.Size = new System.Drawing.Size(25, 25);
            this.secondboard.TabIndex = 64;
            this.secondboard.UseVisualStyleBackColor = false;
            this.secondboard.Click += new System.EventHandler(this.secondboard_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(351, 298);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(57, 13);
            this.label24.TabIndex = 63;
            this.label24.Text = "Technicks";
            // 
            // technicks
            // 
            this.technicks.Location = new System.Drawing.Point(320, 292);
            this.technicks.Name = "technicks";
            this.technicks.Size = new System.Drawing.Size(25, 25);
            this.technicks.TabIndex = 62;
            this.technicks.UseVisualStyleBackColor = false;
            this.technicks.Click += new System.EventHandler(this.technicks_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(351, 267);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(66, 13);
            this.label25.TabIndex = 61;
            this.label25.Text = "Gambit Slots";
            // 
            // gambits
            // 
            this.gambits.Location = new System.Drawing.Point(320, 261);
            this.gambits.Name = "gambits";
            this.gambits.Size = new System.Drawing.Size(25, 25);
            this.gambits.TabIndex = 60;
            this.gambits.UseVisualStyleBackColor = false;
            this.gambits.Click += new System.EventHandler(this.gambits_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(351, 236);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(54, 13);
            this.label26.TabIndex = 59;
            this.label26.Text = "Augments";
            // 
            // augments
            // 
            this.augments.Location = new System.Drawing.Point(320, 230);
            this.augments.Name = "augments";
            this.augments.Size = new System.Drawing.Size(25, 25);
            this.augments.TabIndex = 58;
            this.augments.UseVisualStyleBackColor = false;
            this.augments.Click += new System.EventHandler(this.augments_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(351, 205);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(84, 13);
            this.label27.TabIndex = 57;
            this.label27.Text = "Arcane Magicks";
            // 
            // arcane
            // 
            this.arcane.Location = new System.Drawing.Point(320, 199);
            this.arcane.Name = "arcane";
            this.arcane.Size = new System.Drawing.Size(25, 25);
            this.arcane.TabIndex = 56;
            this.arcane.UseVisualStyleBackColor = false;
            this.arcane.Click += new System.EventHandler(this.arcane_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(351, 174);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(79, 13);
            this.label28.TabIndex = 55;
            this.label28.Text = "Green Magicks";
            // 
            // green
            // 
            this.green.Location = new System.Drawing.Point(320, 168);
            this.green.Name = "green";
            this.green.Size = new System.Drawing.Size(25, 25);
            this.green.TabIndex = 54;
            this.green.UseVisualStyleBackColor = false;
            this.green.Click += new System.EventHandler(this.green_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(351, 143);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(73, 13);
            this.label29.TabIndex = 53;
            this.label29.Text = "Time Magicks";
            // 
            // time
            // 
            this.time.Location = new System.Drawing.Point(320, 137);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(25, 25);
            this.time.TabIndex = 52;
            this.time.UseVisualStyleBackColor = false;
            this.time.Click += new System.EventHandler(this.time_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(351, 112);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(77, 13);
            this.label30.TabIndex = 51;
            this.label30.Text = "Black Magicks";
            // 
            // blm
            // 
            this.blm.Location = new System.Drawing.Point(320, 106);
            this.blm.Name = "blm";
            this.blm.Size = new System.Drawing.Size(25, 25);
            this.blm.TabIndex = 50;
            this.blm.UseVisualStyleBackColor = false;
            this.blm.Click += new System.EventHandler(this.blm_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(351, 81);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(78, 13);
            this.label31.TabIndex = 49;
            this.label31.Text = "White Magicks";
            // 
            // whm
            // 
            this.whm.Location = new System.Drawing.Point(320, 75);
            this.whm.Name = "whm";
            this.whm.Size = new System.Drawing.Size(25, 25);
            this.whm.TabIndex = 48;
            this.whm.UseVisualStyleBackColor = false;
            this.whm.Click += new System.EventHandler(this.whm_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(351, 50);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(64, 13);
            this.label32.TabIndex = 47;
            this.label32.Text = "Accessories";
            // 
            // accessories
            // 
            this.accessories.Location = new System.Drawing.Point(320, 44);
            this.accessories.Name = "accessories";
            this.accessories.Size = new System.Drawing.Size(25, 25);
            this.accessories.TabIndex = 46;
            this.accessories.UseVisualStyleBackColor = false;
            this.accessories.Click += new System.EventHandler(this.accessories_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(351, 19);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(67, 13);
            this.label33.TabIndex = 45;
            this.label33.Text = "Mystic Armor";
            // 
            // mystic
            // 
            this.mystic.Location = new System.Drawing.Point(320, 13);
            this.mystic.Name = "mystic";
            this.mystic.Size = new System.Drawing.Size(25, 25);
            this.mystic.TabIndex = 44;
            this.mystic.UseVisualStyleBackColor = false;
            this.mystic.Click += new System.EventHandler(this.mystic_Click);
            // 
            // closeBtn
            // 
            this.closeBtn.Location = new System.Drawing.Point(181, 383);
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.Size = new System.Drawing.Size(75, 23);
            this.closeBtn.TabIndex = 66;
            this.closeBtn.Text = "Close";
            this.closeBtn.UseVisualStyleBackColor = true;
            this.closeBtn.Click += new System.EventHandler(this.closeBtn_Click);
            // 
            // revertDefaults
            // 
            this.revertDefaults.Location = new System.Drawing.Point(165, 354);
            this.revertDefaults.Name = "revertDefaults";
            this.revertDefaults.Size = new System.Drawing.Size(100, 23);
            this.revertDefaults.TabIndex = 67;
            this.revertDefaults.Text = "Revert Defaults";
            this.revertDefaults.UseVisualStyleBackColor = true;
            this.revertDefaults.Click += new System.EventHandler(this.revertDefaults_Click);
            // 
            // ColorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 415);
            this.Controls.Add(this.revertDefaults);
            this.Controls.Add(this.closeBtn);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.secondboard);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.technicks);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.gambits);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.augments);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.arcane);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.green);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.time);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.blm);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.whm);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.accessories);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.mystic);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.light);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.heavy);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.shields);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.handbombs);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.measures);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.maces);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.staves);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.rods);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.daggers);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.axes);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.guns);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.crossbows);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.bows);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.poles);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.spears);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ninjaswords);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.katanas);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.greatswords);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.swords);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.essentials);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.summons);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.quickenings);
            this.Name = "ColorForm";
            this.Text = "Color Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button quickenings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button summons;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button essentials;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button swords;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button greatswords;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button katanas;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button ninjaswords;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button spears;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button poles;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button bows;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button crossbows;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button light;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button heavy;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button shields;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button handbombs;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button measures;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button maces;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button staves;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button rods;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button daggers;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button axes;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button guns;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button secondboard;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button technicks;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button gambits;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button augments;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button arcane;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button green;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button time;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button blm;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button whm;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button accessories;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button mystic;
        private System.Windows.Forms.Button closeBtn;
        private System.Windows.Forms.Button revertDefaults;
    }
}