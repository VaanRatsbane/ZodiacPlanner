﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZodiacPlanner
{
    public partial class CustomLic : Form
    {

        public string selectedPath;
        public bool? loadingFromVbf { get; private set; }
        public string lang;

        public CustomLic()
        {
            InitializeComponent();
            loadingFromVbf = null;
            openVbfDialog.Filter = "vbf files (*.vbf)|*.vbf|All files (*.*)|*.*";
            openBinDialog.Filter = "bin files (*.bin)|*.bin|All files (*.*)|*.*";
            lang = "us";
            if (Program.settings.darkMode)
            {
                BackColor = SystemColors.ControlDarkDark;
                label1.ForeColor = SystemColors.ButtonHighlight;
                english.ForeColor = SystemColors.ButtonHighlight;
                french.ForeColor = SystemColors.ButtonHighlight;
                german.ForeColor = SystemColors.ButtonHighlight;
                italian.ForeColor = SystemColors.ButtonHighlight;
                spanish.ForeColor = SystemColors.ButtonHighlight;
                korean.ForeColor = SystemColors.ButtonHighlight;
                japanese.ForeColor = SystemColors.ButtonHighlight;
                tradchinese.ForeColor = SystemColors.ButtonHighlight;
                simplechinese.ForeColor = SystemColors.ButtonHighlight;
            }
        }

        private void loadVBF_Click(object sender, EventArgs e)
        {
            if(openVbfDialog.ShowDialog() == DialogResult.OK)
            {
                selectedPath = openVbfDialog.FileName;
                loadingFromVbf = true;
                loadBtn.Enabled = true;
            }
        }

        private void loadBP_Click(object sender, EventArgs e)
        {
            if (openBinDialog.ShowDialog() == DialogResult.OK)
            {
                selectedPath = openBinDialog.FileName;
                loadingFromVbf = false;
                loadBtn.Enabled = true;
            }
        }

        private void loadBtn_Click(object sender, EventArgs e)
        {
            if (english.Checked) lang = "us";
            else if (french.Checked) lang = "fr";
            else if (italian.Checked) lang = "it";
            else if (german.Checked) lang = "de";
            else if (spanish.Checked) lang = "es";
            else if (japanese.Checked) lang = "jp";
            else if (korean.Checked) lang = "kr";
            else if (simplechinese.Checked) lang = "cn";
            else if (tradchinese.Checked) lang = "ch";
            Close();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            loadingFromVbf = null;
            Close();
        }
    }
}
